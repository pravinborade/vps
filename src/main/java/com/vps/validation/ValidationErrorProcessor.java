package com.vps.validation;

import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;


@Component
public class ValidationErrorProcessor {
	
		public ValidationError processValidationError(Errors errors) {
			List<FieldError> fieldErrors = errors.getFieldErrors();
		    return processFieldErrors(fieldErrors);
		 }
		
		private ValidationError processFieldErrors(List<FieldError> fieldErrors) {
		    ValidationError dto = new ValidationError();
		
		    for (FieldError fieldError: fieldErrors) {
		        dto.addFieldError(fieldError.getField(), fieldError.getDefaultMessage());
		    }	
		    
		    return dto;
		}
}
