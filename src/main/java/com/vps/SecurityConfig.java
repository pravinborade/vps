package com.vps;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@Order(SecurityProperties.BASIC_AUTH_ORDER)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired
	private JwtTokenProvider jwtTokenProvider;
	
	@Autowired
	private UserDetailsService userDetailsService;
	
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;	
	
	@Bean
	public AuthenticationManager authenticationManagerBean() throws Exception {
	      return super.authenticationManagerBean();

	}
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth)
	throws Exception {
	auth
	.userDetailsService(userDetailsService)
	.passwordEncoder(bCryptPasswordEncoder);

	}
	
	@Bean
	public UserDetailsService userDetailsService() {
	    return super.userDetailsService();
	}
	 
	
	
	@Override
    protected void configure(HttpSecurity http) throws Exception {
		http.csrf().disable();  
	      http
	        .authorizeRequests()
	        .antMatchers("/").permitAll()	         
	        .antMatchers("/api/login").permitAll()
	        .antMatchers("/api/student/**").permitAll();
	         
	       http.apply(new JwtTokenFilterConfigurer(jwtTokenProvider));	          
	    }

	}	
	
	

