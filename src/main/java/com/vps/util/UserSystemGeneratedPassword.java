package com.vps.util;

import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;

public class UserSystemGeneratedPassword {		
	public static String CAPITAL_CHARACTER="ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	public static String SMALL_CHARACTER="abcdefghijklmnopqrstuvwxyz";
	public static String NUMBERS="0123456789";
	
	 @Autowired
	 private static PasswordEncoder passwordEncoder;
		
	public static String getSGPassword() {
		int length = 6;	 
		String defaultPassword="";      
		String values = CAPITAL_CHARACTER +SMALL_CHARACTER +NUMBERS; 
		Random rndm_method = new Random();   
		
		char[] password = new char[length];       
			for(int i = 0; i < length; i++) {
				password[i] = values.charAt(rndm_method.nextInt(values.length()));      
			}      
         
			for(char c:password) {
				defaultPassword=defaultPassword+""+Character.toString(c);  
			   // defaultPassword = passwordEncoder.encode(defaultPassword);		
				System.out.println(defaultPassword);     
			}
			
		return defaultPassword; 
	}
}
