package com.vps.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vps.dto.TeacherDTO;
import com.vps.form.TeacherForm;
import com.vps.model.Teacher;
import com.vps.model.TeacherAddress;
import com.vps.respository.TeacherRepository;

@Service
public class TeacherService {
	
	@Autowired
	private TeacherRepository teacherRepository;

	public TeacherDTO addTeacher(TeacherForm form  ) {	
		Teacher teacher = null;
		if(form.getId() > 0) {			
			    Optional<Teacher> teacherid = teacherRepository.findById(form.getId());
				if(teacherid.isPresent()) {									
					teacher = teacherid.get();
				}				
	  }	else {
		  teacher = new Teacher();			
	  	}				
		teacher.setId(form.getId());
		teacher.setFirstName(form.getFirstName());
		teacher.setMiddleName(form.getMiddleName());
		teacher.setLastName(form.getLastName());
		teacher.setBirthDate(form.getBirthDate());
		teacher.setMobileNumber(form.getMobileNumber());
		
		List<TeacherAddress> addressList = new ArrayList<>();
		if (form.getAddresses() != null) {
			for (TeacherAddress ta : form.getAddresses()) {
				TeacherAddress teacheraddress = new TeacherAddress();				
				teacheraddress.setCity(ta.getCity());
				teacheraddress.setDistrict(ta.getDistrict());
				teacheraddress.setState(ta.getState());
				teacheraddress.setPincode(ta.getPincode());
				teacheraddress.setTeacher(teacher);
				addressList.add(teacheraddress);
			}
		}
		teacher.setAddresses(addressList);
		teacher = teacherRepository.save(teacher);
		return teacher.toDTO();	
	}
	
	
	
	
}
