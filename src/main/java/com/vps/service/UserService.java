package com.vps.service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.vps.JwtTokenProvider;
import com.vps.customexception.MyException;
import com.vps.dto.UserDTO;
import com.vps.form.LoginForm;
import com.vps.form.UserForm;
import com.vps.model.Role;
import com.vps.model.User;
import com.vps.respository.RoleRepository;
import com.vps.respository.UserRepository;
import com.vps.util.UserSystemGeneratedPassword;

@Service
public class UserService implements UserDetailsService{
	
	 public static String DEFAULTROLE="ADMIN";
	
	 @Autowired
	 private PasswordEncoder passwordEncoder;
	 
	 @Autowired
	 private UserRepository userRepository;
	 
	 @Autowired
	 private RoleRepository roleRepository;
	 
	 @Autowired
	 private AuthenticationManager authenticationManager;	 
	 
	 @Autowired
	 private JwtTokenProvider jwtTokenProvider;
	 
	 @Autowired
	 private ImageService imageService;
	 
	 
	 
	
	
	public String login(LoginForm loginFom) {
		try {
		authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginFom.getPhoneNumber(), loginFom.getPassword()));
		return jwtTokenProvider.createToken(loginFom.getPhoneNumber(),
		new HashSet<>(userRepository.findByPhoneNumber(loginFom.getPhoneNumber()).getRole()));
		} catch (AuthenticationException e) {
		throw new MyException("Invalid phone number or password", HttpStatus.UNPROCESSABLE_ENTITY);
		}
	}

	 @Transactional
	public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
		    User user = userRepository.findByPhoneNumber(userName);
			Set<GrantedAuthority> authorities = getUserAuthority(user.getRole());
			if (authorities == null) {
			authorities = new HashSet<GrantedAuthority>();
			}
			UserDetails userDetails = buildUserForAuthentication(user, authorities);
			return userDetails;
	}
	 
	 private Set<GrantedAuthority> getUserAuthority(Set<Role> userRoles) {
		 Set<GrantedAuthority> roles = new HashSet<GrantedAuthority>();
		 for (Role role : userRoles) {
		 roles.add(new SimpleGrantedAuthority(role.getRole().toString()));
		 }

		 Set<GrantedAuthority> grantedAuthorities = new HashSet<GrantedAuthority>(roles);
		 return grantedAuthorities;
		 }
	
	private UserDetails buildUserForAuthentication(User user, Set<GrantedAuthority> authorities) {
		return new org.springframework.security.core.userdetails.User(
		user.getPhoneNumber(), user.getPassword(),
		true, true, true, true, authorities);
		}
	
	
	public User findByToken(String authenticationToken) {		
		return userRepository.findByAuthenticationToken(authenticationToken);	
    }
	
	public User createUser(UserForm form) {
		User user =new User();
		user.setFirstName(form.getFirstName());
		user.setLastName(form.getLastName());
		user.setPhoneNumber(form.getPhoneNumber());		
		String hashedPassword = passwordEncoder.encode(UserSystemGeneratedPassword.getSGPassword());		
		user.setPassword(hashedPassword);
		Date date=new Date();			
		user.setCreatedDate(new Timestamp(date.getTime()));
		user.setSystemGenratedPassword(true);	
			
//		String path=user.getFirstName()+""+user.getLastName();					
//		MultipartFile m = null;
//		String imageUrl =imageService.convertToUserImage(m, path);	
//		user.setImage(imageUrl);
		
		Role roles=roleRepository.findByRole(DEFAULTROLE);
		Set<Role> roleSets=new HashSet<>();
		if(roles == null) {
			Role role=new Role();
			role.setRole(DEFAULTROLE);
		    roles=roleRepository.save(role);
		}		
		roleSets.add(roles);
		user.setRole(roleSets);
		user = userRepository.save(user);		
		return user;			
	}
	
	public UserDTO updateUser(UserForm form ,User user) {	
		user.setFirstName(form.getFirstName());
		user.setLastName(form.getLastName());
		user.setAuthenticationToken(form.getAuthenticationToken());
		user.setSystemGenratedPassword(true);
		user.setId(form.getId());
		user.setPhoneNumber(form.getPhoneNumber());
		user.setPassword(form.getPassword());		
		String hashedPassword = passwordEncoder.encode(form.getPassword());		
		user.setPassword(hashedPassword);		
		Date date=new Date();			
		user.setCreatedDate(new Timestamp(date.getTime()));	
		
		//MultipartFile m = null;
		String path=user.getFirstName()+""+user.getLastName();				
		String imageUrl =imageService.convertToUserImage(form.getImage(), path);	
		user.setImage(imageUrl);
		

		Role roles=roleRepository.findByRole(DEFAULTROLE);
		Set<Role> roleSets=new HashSet<>();
		if(roles == null) {
			Role role=new Role();
			role.setRole(DEFAULTROLE);
		    roles=roleRepository.save(role);
		}		
		roleSets.add(roles);
		user.setRole(roleSets);
		user = userRepository.save(user);		
		return user.toDTO();	
	}
}
	
