package com.vps.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.mysql.cj.ParseInfo;
import com.vps.dto.StudentDTO;
import com.vps.form.StudentForm;
import com.vps.form.StudentParentForm;
import com.vps.model.Student;
import com.vps.model.StudentAddress;
import com.vps.model.StudentImage;
import com.vps.model.StudentParent;
import com.vps.respository.StudentRepository;

@Service
public class StudentService {
	@Autowired
	private StudentRepository studentRepository;

	@Autowired
	private ImageService imageService;

	public StudentDTO addStudent(StudentForm form) {
		Student student = null;
		
		
		if (form.getId() > 0) {
			Optional<Student> stdudent = studentRepository.findById(form.getId());
			if (stdudent.isPresent()) {
				student = stdudent.get();

			}
		} else {
			student = new Student();
		}
		student.setId(form.getId());
		student.setFirstName(form.getFirstName());
		student.setMiddleName(form.getMiddleName());
		student.setLastName(form.getLastName());
		student.setBirthDate(form.getBirthDate());
		student.setBirthPlace(form.getBirthPlace());
		student.setGender(form.getGender());
		student.setReligion(form.getReligion());
		student.setCaste(form.getCaste());
		student.setSubCaste(form.getSubCaste());
		student.setNationality(form.getNationality());

		if (form.getStudentParent() != null) {
			StudentParentForm studentparentform = form.getStudentParent();
			StudentParent studentParent = student.getStudentParent();
			if (studentParent == null) {
				studentParent = new StudentParent();
			}
			studentParent.setFirstName(studentparentform.getFirstName());
			studentParent.setMiddleName(studentparentform.getMiddleName());
			studentParent.setLastName(studentparentform.getLastName());
			studentParent.setGuardian(studentparentform.getGuardian());
			studentParent.setStudent(student);
			student.setStudentParent(studentParent);
		}

		List<StudentImage> imglist = new ArrayList<>();
		if (form.getImages() != null) {
			int count = 0;
			for (MultipartFile m : form.getImages()) {

				String path = student.getFirstName() + "" + student.getLastName() + "_" + (count++) + "_";

				String imageUrl = imageService.convertToStudentImage(m, path);
				StudentImage studentimg = new StudentImage();
				studentimg.setImage(imageUrl);
				studentimg.setStudent(student);
				imglist.add(studentimg);
			}
		}
		student.setImages(imglist);

		List<StudentAddress> addressList = new ArrayList<>();
		if (form.getAddresses() != null) {
			for (StudentAddress sa : form.getAddresses()) {
				StudentAddress studentaddress = new StudentAddress();
				studentaddress.setCity(sa.getCity());
				studentaddress.setDistrict(sa.getDistrict());
				studentaddress.setState(sa.getState());
				studentaddress.setPincode(sa.getPincode());
				studentaddress.setStudent(student);
				addressList.add(studentaddress);
			}
		}
		student.setAddresses(addressList);
		student = studentRepository.save(student);
		return student.toDTO();
	}
}
