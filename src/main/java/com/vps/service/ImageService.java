package com.vps.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class ImageService {	
	
	public String convertToStudentImage(MultipartFile image,String url) {
		String path = null;
	    try {		
			File file = File.createTempFile(url, ".jpeg");
			 OutputStream  os = new FileOutputStream(file);
			  path =file.getAbsolutePath();
			 byte[] bytes = image.getBytes(); 	
			 os.write(bytes);
			 os.close();
		}catch(Exception e){
			e.printStackTrace();
		}		
	   return path;
	}
	
	public String convertToUserImage(MultipartFile image,String url) {
		String path = null;
	    try {		
			File file = File.createTempFile(url, ".jpeg");
			 OutputStream  os = new FileOutputStream(file);
			 path =file.getAbsolutePath();
			 byte[] bytes = image.getBytes(); 	
			 os.write(bytes);
			 os.close();
		}catch(Exception e){
			e.printStackTrace();
		}		
	   return path;
	}
	
	
	
	
}
