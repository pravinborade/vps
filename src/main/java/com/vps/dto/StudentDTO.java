package com.vps.dto;

import java.util.List;

public class StudentDTO {
	private int id;
	private String firstName;
	private String middleName;
	private String lastName;
	private String birthDate;
	private String birthPlace;
	private String gender;
	private String religion;
	private String caste;
	private String subCaste;
	private String nationality;
	private StudentParentDTO studentParent;
	private List<StudentImageDTO> studentImage;
	private List<StudentAddressDTO> studentAddress;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	public String getBirthPlace() {
		return birthPlace;
	}

	public void setBirthPlace(String birthPlace) {
		this.birthPlace = birthPlace;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getReligion() {
		return religion;
	}

	public void setReligion(String religion) {
		this.religion = religion;
	}

	public String getCaste() {
		return caste;
	}

	public void setCaste(String caste) {
		this.caste = caste;
	}

	public String getSubCaste() {
		return subCaste;
	}

	public void setSubCaste(String subCaste) {
		this.subCaste = subCaste;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public StudentParentDTO getStudentParent() {
		return studentParent;
	}

	public void setStudentParent(StudentParentDTO studentParent) {
		this.studentParent = studentParent;
	}

	public List<StudentImageDTO> getStudentImage() {
		return studentImage;
	}

	public void setStudentImage(List<StudentImageDTO> studentImage) {
		this.studentImage = studentImage;
	}

	public List<StudentAddressDTO> getStudentAddress() {
		return studentAddress;
	}

	public void setStudentAddress(List<StudentAddressDTO> studentAddress) {
		this.studentAddress = studentAddress;
	}

}
