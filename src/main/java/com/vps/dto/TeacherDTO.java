package com.vps.dto;

import java.sql.Timestamp;
import java.util.List;

public class TeacherDTO {

	private int id;
	private String firstName;
	private String middleName;
	private String lastName;
	private String birthdate;
	private String mobileNumber;
	private List<TeacherAddressDTO> teacherAddress;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	
	public String getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(String birthdate) {
		this.birthdate = birthdate;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public List<TeacherAddressDTO> getTeacherAddress() {
		return teacherAddress;
	}

	public void setTeacherAddress(List<TeacherAddressDTO> teacherAddress) {
		this.teacherAddress = teacherAddress;
	}

}
