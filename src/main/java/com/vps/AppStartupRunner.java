package com.vps;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.vps.model.Role;
import com.vps.model.User;
import com.vps.respository.RoleRepository;
import com.vps.respository.UserRepository;
@Component
public class AppStartupRunner implements ApplicationRunner{
	public static String DEFAULTROLE="ADMIN";
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private RoleRepository roleRepository;
	
	@Autowired
	private PasswordEncoder passwordEncoder;

	@Override
	@Transactional
	public void run(ApplicationArguments args) throws Exception {
		
		User user = userRepository.findByPhoneNumber("7507554473");
		if(user == null){
			user = new User();
			user.setPhoneNumber("7507554473");			
			Date date=new Date();			
			user.setCreatedDate(new Timestamp(date.getTime()));
			String hashedPassword = passwordEncoder.encode("123456");	
			user.setPassword(hashedPassword);
			user.setFirstName("Pravin");
			user.setLastName("Borade");
			user.setSystemGenratedPassword(false);
			Role roles=roleRepository.findByRole(DEFAULTROLE);
			Set<Role> roleSets=new HashSet();
			if(roles == null) {
				roles=new Role();
				roles.setRole(DEFAULTROLE);
				roles=roleRepository.save(roles);
			}
			user = userRepository.save(user);
		}
		
	}

}