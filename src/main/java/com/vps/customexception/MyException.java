package com.vps.customexception;

import org.springframework.http.HttpStatus;

public class MyException extends RuntimeException {
	public String message;
	public HttpStatus status;	
	
	public MyException(String message,HttpStatus status){
		this.message=message;
		this.status=status;
	}

}
