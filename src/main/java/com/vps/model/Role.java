package com.vps.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.springframework.security.core.GrantedAuthority;

import com.vps.dto.RoleDTO;

@Entity
@Table(name = "role")
public class Role implements GrantedAuthority {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int id;

	@Column(name = "role")
	private String role;

	@ManyToMany(mappedBy = "role")
    private Set<User> user = new HashSet<>();
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}
	public RoleDTO toDTO() {
		RoleDTO dto = new RoleDTO();
		dto.setId(this.id);
		dto.setRole(this.role);
		return dto;		
	}
	@Override
	public String getAuthority() {
	// TODO Auto-generated method stub
	return role;
	}
}
