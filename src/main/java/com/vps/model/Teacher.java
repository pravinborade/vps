package com.vps.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.vps.dto.TeacherAddressDTO;
import com.vps.dto.TeacherDTO;

@Entity
@Table(name = "teacher")
public class Teacher {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int id;

	@Column(name = "first_name")
	private String firstName;

	@Column(name = "middle_name")
	private String middleName;

	@Column(name = "last_name")
	private String lastName;

	@Column(name = "birth_date")
	private String birthDate;
	//1970-01-19T03:57:46.406+0000
	
	@Column(name = "mobile_number")
	private String mobileNumber;
	
	@OneToMany(mappedBy = "teacher", cascade = CascadeType.ALL)
	private List<TeacherAddress> addresses;     

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	
	public List<TeacherAddress> getAddresses() {
		return addresses;
	}

	public void setAddresses(List<TeacherAddress> addresses) {
		this.addresses = addresses;
	}

	public TeacherDTO toDTO() {
		TeacherDTO dto = new TeacherDTO();
		dto.setId(this.id);
		dto.setFirstName(this.firstName);
		dto.setMiddleName(this.middleName);
		dto.setLastName(this.lastName);
		dto.setBirthdate(this.birthDate);
		dto.setMobileNumber(this.mobileNumber);
		if(addresses != null) {
			List<TeacherAddressDTO> dtoList = new ArrayList<>();
			for(TeacherAddress ta: addresses) {
				dtoList.add(ta.toDTO());
			}
			dto.setTeacherAddress(dtoList);
		}
		return dto;
	}
}

