package com.vps.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.vps.dto.StudentAddressDTO;
import com.vps.dto.StudentDTO;
import com.vps.dto.StudentImageDTO;

@Entity
@Table(name = "student")
public class Student {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)

	@Column(name = "id")
	private int id;

	@Column(name = "first_name")
	private String firstName;

	@Column(name = "middle_name")
	private String middleName;

	@Column(name = "last_name")
	private String lastName;

	@Column(name = "birth_date")
	private String birthDate;

	@Column(name = "birth_place")
	private String birthPlace;

	@Column(name = "gender")
	private String gender;

	@Column(name = "religion")
	private String religion;

	@Column(name = "caste")
	private String caste;

	@Column(name = "sub_caste")
	private String subCaste;

	@Column(name = "nationality")
	private String nationality;

	@OneToOne(mappedBy = "student", cascade = CascadeType.ALL)
	private StudentParent studentParent;

	@OneToMany(mappedBy = "student", cascade = CascadeType.ALL)
	private List<StudentImage> images;

	@OneToMany(mappedBy = "student", cascade = CascadeType.ALL)
	private List<StudentAddress> addresses;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	public String getBirthPlace() {
		return birthPlace;
	}

	public void setBirthPlace(String birthPlace) {
		this.birthPlace = birthPlace;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getReligion() {
		return religion;
	}

	public void setReligion(String religion) {
		this.religion = religion;
	}

	public String getCaste() {
		return caste;
	}

	public void setCaste(String caste) {
		this.caste = caste;
	}

	public String getSubCaste() {
		return subCaste;
	}

	public void setSubCaste(String subCaste) {
		this.subCaste = subCaste;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public StudentParent getStudentParent() {
		return studentParent;
	}

	public List<StudentImage> getImages() {
		return images;
	}

	public void setImages(List<StudentImage> images) {
		this.images = images;
	}

	public void setStudentParent(StudentParent studentParent) {
		this.studentParent = studentParent;
	}

	public List<StudentAddress> getAddresses() {
		return addresses;
	}

	public void setAddresses(List<StudentAddress> addresses) {
		this.addresses = addresses;
	}

	public StudentDTO toDTO() {
		StudentDTO dto = new StudentDTO();

		dto.setId(this.id);
		dto.setFirstName(this.firstName);
		dto.setMiddleName(this.middleName);
		dto.setLastName(this.lastName);
		dto.setBirthDate(this.birthDate);
		dto.setBirthPlace(this.birthPlace);
		dto.setGender(this.gender);
		dto.setReligion(this.religion);
		dto.setCaste(this.caste);
		dto.setSubCaste(this.subCaste);
		dto.setNationality(this.nationality);
		
		if (studentParent != null) {
			dto.setStudentParent(studentParent.toDTO());
		}
		
		if (images != null) {
			List<StudentImageDTO> dtoList = new ArrayList<>();
			for (StudentImage si : images) {
				dtoList.add(si.toDTO());
			}
			dto.setStudentImage(dtoList);
		}
		
		if (addresses != null) {
			List<StudentAddressDTO> dtoList = new ArrayList<>();
			for (StudentAddress sa : addresses) {
				dtoList.add(sa.toDTO());
			}
			dto.setStudentAddress(dtoList);
		}
		
		return dto;
	}

}
