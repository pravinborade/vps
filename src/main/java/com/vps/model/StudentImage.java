package com.vps.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.vps.dto.StudentImageDTO;

@Entity
@Table(name = "student_image")
public class StudentImage {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int id;

	@Column(name = "image")
	private String image;
	
	@ManyToOne
    @JoinColumn(name = "student_id")
    private Student student;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public StudentImageDTO toDTO() {

		StudentImageDTO dto = new StudentImageDTO();

		dto.setId(this.id);
		dto.setImage(this.image);
		
		return dto;

	}

}
