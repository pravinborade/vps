package com.vps.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import org.springframework.web.multipart.MultipartFile;

import com.vps.dto.UserDTO;

@Entity
@Table(name = "user")
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int id;

	@NotBlank(message = "PhoneNumber cannot be blank")
	@Column(name = "phone_number")
	private String phoneNumber;

	@NotBlank(message = "Password cannot be blank")
	@Column(name = "password")
	private String password;

	@Column(name = "signup_token")
	private String signUpToken;

	@Column(name = "created_date")
	private Timestamp createdDate;

	@Column(name = "authentication_token")
	private String authenticationToken;

	@Column(name = "first_name")
	private String firstName;

	@Column(name = "last_name")
	private String lastName;

	@Column(name = "is_system_generated_password")
	private boolean isSystemGenratedPassword;
	
	@Column(name = "image")
	private String image;

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "user_role", joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"))
	private Set<Role> role;

	public Set<Role> getRole() {
		return role;
	}

	public void setRole(Set<Role> role) {
		this.role = role;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSignUpToken() {
		return signUpToken;
	}

	public void setSignUpToken(String signUpToken) {
		this.signUpToken = signUpToken;
	}

	public Timestamp getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public String getAuthenticationToken() {
		return authenticationToken;
	}

	public void setAuthenticationToken(String authenticationToken) {
		this.authenticationToken = authenticationToken;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public boolean isSystemGenratedPassword() {
		return isSystemGenratedPassword;
	}

	public void setSystemGenratedPassword(boolean isSystemGenratedPassword) {
		this.isSystemGenratedPassword = isSystemGenratedPassword;
	}

	

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public UserDTO toDTO() {
		UserDTO dto = new UserDTO();
		dto.setId(this.id);
		dto.setPhone_Number(this.phoneNumber);
		dto.setPassword(this.password);
		dto.setAuthenticationToken(this.authenticationToken);
		dto.setFirstName(this.firstName);
		dto.setLastName(this.lastName);
		dto.setSystemGeneratedPassword(this.isSystemGenratedPassword);
		dto.setImage(this.image);
		
		List<Role> roleDTOList = new ArrayList();

		if (role != null) {
			for (Role r : role) {
				dto.setRole(r.toDTO());
			}
		}
		return dto;
	}
}
