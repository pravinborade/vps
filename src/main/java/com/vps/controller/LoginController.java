package com.vps.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.vps.customexception.MyException;
import com.vps.dto.AuthenticationTokenDTO;
import com.vps.form.LoginForm;
import com.vps.model.User;
import com.vps.respository.UserRepository;
import com.vps.service.UserService;

@Controller
public class LoginController {

	@Autowired
	public UserRepository userRepository;

	@Autowired
	public UserService userService;		
	
	@PostMapping(value="/api/login")	
	public ResponseEntity<?> login(@Valid @RequestBody LoginForm form,Errors error) {
		
		AuthenticationTokenDTO dto =new AuthenticationTokenDTO();
		try {
		User user = userRepository.findByPhoneNumber(form.getPhoneNumber());			
		if(user == null) {
				throw new MyException("invalid phone number or passowrd",HttpStatus.BAD_REQUEST);
			}			
		String authenticationToken=userService.login(form);	
		user.setAuthenticationToken(authenticationToken);
		user = userRepository.save(user);
		dto.setAuthenticationToken(authenticationToken);		
		}catch(MyException e) {
				return ResponseEntity.badRequest().body(e.getMessage());	
			}		
		return ResponseEntity.ok().body(dto);	
	}
	
	@GetMapping("/api/login/token/{authenticationToken}")
	public ResponseEntity<?> findUserByAuthentication(@PathVariable String authenticationToken){
		User user =new User();
		try {
		 user = userService.findByToken(authenticationToken);
		 if(user == null) {
			 throw new MyException("invalid user",HttpStatus.BAD_REQUEST);
		 }
		}catch(Exception e){
			return ResponseEntity.badRequest().body(e.getMessage());
		}
		return ResponseEntity.ok(user.toDTO());		
	}			
}
