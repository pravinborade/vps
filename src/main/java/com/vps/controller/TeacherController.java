package com.vps.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vps.dto.PageableTeacherDTO;
import com.vps.dto.StudentDTO;
import com.vps.dto.TeacherDTO;
import com.vps.form.StudentForm;
import com.vps.form.TeacherForm;
import com.vps.model.Teacher;
import com.vps.respository.TeacherRepository;
import com.vps.service.TeacherService;

@RestController
@RequestMapping("/api/teacher")
public class TeacherController {
	
	@Autowired
	private TeacherRepository teacherRepository;
	
	@Autowired
	private TeacherService teacherService;
	
	@PostMapping(value = "/add" ,consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public  ResponseEntity<?> addTeacherInfo(TeacherForm form) {
		TeacherDTO teacher = new TeacherDTO();
		teacher = teacherService.addTeacher(form);
		return ResponseEntity.ok(teacher);
	}
	
	@PostMapping(value ="/update",consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> updateTeacherInfo(TeacherForm form) {
		TeacherDTO teacher = new TeacherDTO();
		teacher = teacherService.addTeacher(form);
		return ResponseEntity.ok(teacher);
	}

//	@GetMapping("/all/{size}/{pagenumber}")
//	public ResponseEntity<?> findAll(@PathVariable("size") int size,@PathVariable("pagenumber") int pagenumber) {
//		Pageable pageable = PageRequest.of(pagenumber, size,Sort.by("id").ascending());		
//		Page<Teacher> teachers = teacherRepository.findAll(pageable);
//		List<TeacherDTO> teacherDTOs = new ArrayList<TeacherDTO>();
//		PageableTeacherDTO dto=new PageableTeacherDTO();
//		if (teachers != null) {
//			for (Teacher t : teachers) {
//				teacherDTOs.add(t.toDTO());
//			}			
//		} 
//		dto.setTeachers(teacherDTOs);
//		dto.setTotalElements(teachers.getTotalElements());
//		dto.setTotalPages(teachers.getTotalPages());		
//		return ResponseEntity.ok(dto);
	
		
	@GetMapping("/all")
	public ResponseEntity<?> allTeacherInfo() {
		List<Teacher> teacherall = teacherRepository.findAll();
		List<TeacherDTO> teacherdtolist = new ArrayList<TeacherDTO>();
		if (teacherall != null) {
			for (Teacher t : teacherall) {
				teacherdtolist.add(t.toDTO());
				}
			return ResponseEntity.ok(teacherdtolist);
		} else {
			return ResponseEntity.badRequest().body("Record Not Found...");
		}
	}

	@GetMapping("/{id}")
	public ResponseEntity<?> findByIdTeacherInfo(@PathVariable int id) {
			if (id > 0) {
			Optional<Teacher> teacherid = teacherRepository.findById(id);
			if (teacherid.isPresent()) {
				Teacher teacher = teacherid.get();
				return ResponseEntity.ok(teacher.toDTO());
			}
		}
		return ResponseEntity.badRequest().body("Record Not Found...");
	}
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<?> deleteByIdTeacher(@PathVariable int id) {
		Optional<Teacher> teacherid = teacherRepository.findById(id);
		if (teacherid.isPresent()) {
			teacherRepository.deleteById(id);
			return ResponseEntity.ok("Record Deleted Successfully");
			}
		return ResponseEntity.badRequest().body("Record Not Found...");
		}
}
