package com.vps.controller;

import java.util.Optional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vps.customexception.MyException;
import com.vps.dto.UserDTO;
import com.vps.form.UserForm;
import com.vps.model.User;
import com.vps.respository.UserRepository;
import com.vps.service.UserService;
import com.vps.validation.ValidationError;
import com.vps.validation.ValidationErrorProcessor;

@RestController
@RequestMapping("/api/user")
public class UserController {
	
	@Autowired
	private ValidationErrorProcessor validationErrorProcessor;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private UserRepository userRepository;
	
	@PostMapping(value="/update",consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> updateUser(@Valid UserForm form, Errors error) {		
		

			Optional<User> user = userRepository.findById(form.getId());			
			if(!user.isPresent()){
				throw new MyException("user not found", HttpStatus.BAD_REQUEST);
			}	
		

			UserDTO userDTO = userService.updateUser(form ,user.get());	
			return ResponseEntity.ok(userDTO);
	}
	
	@PostMapping("/create")
	public ResponseEntity<Object> createUser(@Valid  @RequestBody UserForm form, Errors error){	
		if(error.hasErrors()) {
			ValidationError validationError = validationErrorProcessor.processValidationError(error);
			return new ResponseEntity<Object>(validationError, HttpStatus.BAD_REQUEST);
		}				
		User user = userRepository.findByPhoneNumber(form.getPhoneNumber());		
		if(user != null){
			error.rejectValue("phoneNumber", "phoneNumber","phone number is already available");				
		}			
		if(error.hasErrors()) {
			ValidationError validationError = validationErrorProcessor.processValidationError(error);
			return new ResponseEntity<Object>(validationError, HttpStatus.BAD_REQUEST);
		}			
		user =userService.createUser(form);
		return ResponseEntity.ok(user.toDTO());		
	}	

}
