package com.vps.controller;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vps.dto.PageableStudentDTO;
import com.vps.dto.StudentDTO;
import com.vps.form.StudentForm;
import com.vps.model.Student;
import com.vps.respository.StudentRepository;
import com.vps.service.StudentService;

@RestController
@RequestMapping("/api/student")
public class StudentController {
	@Autowired
	private StudentRepository studentRepository;
	
	@Autowired
	private StudentService studentService;

	@PostMapping(value = "/add" ,consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> addStudentInfo(StudentForm form) {
		StudentDTO student = new StudentDTO();
		student = studentService.addStudent(form);
		return ResponseEntity.ok(student);
	}

//	@GetMapping("/all/{size}/{pagenumber}")
//	public ResponseEntity<?> findAll(@PathVariable("size") int size,@PathVariable("pagenumber") int pagenumber) {
//		Pageable pageable = PageRequest.of(pagenumber, size,Sort.by("id").ascending());		
//		Page<Student> students = studentRepository.findAll(pageable);
//		List<StudentDTO> studentDTOs = new ArrayList<StudentDTO>();
//		PageableStudentDTO dto=new PageableStudentDTO();
//		if (students != null) {
//			for (Student s : students) {
//				studentDTOs.add(s.toDTO());
//			}			
//		} 
//		dto.setStudents(studentDTOs);
//		dto.setTotalElements(students.getTotalElements());
//		dto.setTotalPages(students.getTotalPages());		
//		return ResponseEntity.ok(dto);		
//	}
	
	@GetMapping("/all")
	public ResponseEntity<?> allStudentInfo() {
			List<Student> studentall = studentRepository.findAll();	
			List<StudentDTO> studentdtolist = new ArrayList<StudentDTO>();
			if (studentall != null) {
				for (Student s : studentall) {
					studentdtolist.add(s.toDTO());
				}
				return ResponseEntity.ok(studentdtolist);
			} else {
				return ResponseEntity.badRequest().body("Record Not Found...");
			}
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<?> findByIdStudentInfo(@PathVariable int id) {
		if (id > 0) {
			Optional<Student> studentid = studentRepository.findById(id);
			if (studentid.isPresent()) {
				Student student = studentid.get();
				return ResponseEntity.ok().body(student.toDTO());
			}
		}
		return ResponseEntity.badRequest().body("Record Not Found...");
	}
	
	@PostMapping(value ="/update",consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> updateStudentInfo(StudentForm form) {
		StudentDTO student = new StudentDTO();
		student = studentService.addStudent(form);
		return ResponseEntity.ok(student);
	}
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<?> deleteByIdStudent(@PathVariable int id) {
			Optional<Student> studentid = studentRepository.findById(id);
			if (studentid.isPresent()) {
				studentRepository.deleteById(id);	

				return ResponseEntity.ok("Record Deleted Successfully");
			}
		return ResponseEntity.badRequest().body("Record Not Found...");
	}
}

