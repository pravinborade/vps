package com.vps.respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vps.model.Role;

public interface RoleRepository extends JpaRepository<Role, Integer> {

	Role findByRole(String role);

}
