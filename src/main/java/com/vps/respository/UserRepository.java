package com.vps.respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vps.model.User;

public interface UserRepository extends JpaRepository<User, Integer> {

	User findByPhoneNumber(String phoneNumber) ;
	User findByAuthenticationToken(String authenticationToken);
}
