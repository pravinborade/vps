package com.vps.respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vps.model.Teacher;

public interface TeacherRepository extends JpaRepository<Teacher, Integer>{

}
