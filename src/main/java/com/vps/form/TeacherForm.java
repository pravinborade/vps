package com.vps.form;

import java.util.List;

import com.vps.model.TeacherAddress;

public class TeacherForm {

	private int id;
	private String firstName;
	private String middleName;
	private String lastName;
	private String birthDate;
	private String mobileNumber;
	private List<TeacherAddress> addresses;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}	

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public List<TeacherAddress> getAddresses() {
		return addresses;
	}

	public void setAddresses(List<TeacherAddress> addresses) {
		this.addresses = addresses;
	}


}
