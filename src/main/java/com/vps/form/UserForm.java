package com.vps.form;

import java.sql.Timestamp;

import org.springframework.web.multipart.MultipartFile;

public class UserForm {

	private int id;
	private String phoneNumber;
	private String password;
	private String firstName;
	private String lastName;
	private String signUpToken;
	private Timestamp createdDate;
	private String authenticationToken;
	private boolean isSystemGenratedPassword;
	private MultipartFile image;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getSignUpToken() {
		return signUpToken;
	}

	public void setSignUpToken(String signUpToken) {
		this.signUpToken = signUpToken;
	}

	public Timestamp getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public String getAuthenticationToken() {
		return authenticationToken;
	}

	public void setAuthenticationToken(String authenticationToken) {
		this.authenticationToken = authenticationToken;
	}

	public boolean isSystemGenratedPassword() {
		return isSystemGenratedPassword;
	}

	public void setSystemGenratedPassword(boolean isSystemGenratedPassword) {
		this.isSystemGenratedPassword = isSystemGenratedPassword;
	}

	public MultipartFile getImage() {
		return image;
	}

	public void setImage(MultipartFile image) {
		this.image = image;
	}
	

}
