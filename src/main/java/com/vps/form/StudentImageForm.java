package com.vps.form;

import org.springframework.web.multipart.MultipartFile;

import com.mysql.cj.jdbc.Blob;

public class StudentImageForm {
	private int id;
	private MultipartFile image;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public MultipartFile getImage() {
		return image;
	}

	public void setImage(MultipartFile image) {
		this.image = image;
	}

}
