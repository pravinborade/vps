import * as React from 'react';
import { Provider as PaperProvider } from 'react-native-paper';
import Theme from './Theme/Theme';

export default function Main() {
  return (
    <PaperProvider>
      <App />
    </PaperProvider>
  );
}