import  {combineReducers} from 'redux'

import { reducer as formReducer } from 'redux-form'
import authReducer from './authReducer';
import studentReducer from './studentReducer';
import addressReducer from './addressReducer';
import feesReducer from './feesReducer';
import studentParentReducer from './studentParent';
import staffReducer from './staffReducer';
import generalDetailReducer from './generalDetailReducer';
import contactReducer from './contactReducer';


const rootReducer = combineReducers({
    form:formReducer,
    auth:authReducer,
    parent:studentParentReducer,
    student:studentReducer,
    staff:staffReducer,
    address:addressReducer,
    fees:feesReducer,
    generalDetail:generalDetailReducer,
    contact:contactReducer,
    
})


export default rootReducer