const addressReducer = (state = {


  address: {
    home: 'LANE NUMBER 10 ',
    city: 'PUNE',
    state: 'MAHARASTRA',
      },
      addressForm: {
        home:'',
        city:'',
        state:'',

    }
},
  action) => {
  switch (action.type) {


    case "ADD_ADDRESS":
      return {
        ...state,
        address: {

          home: action.address.home,
          city: action.address.city,
          state: action.address.state,
        },
        addressForm:{
          home: action.address.home,
          city: action.address.city,
          state: action.address.state,
        }
      }
    case "UPDATE_ADDRESS":
      return {

        ...state,
        address: {
          home: action.address.home,
          city: action.address.city,
          state: action.address.state,
        },

      }
    case "DELETE_ADDRESS":
      return {

        ...state,
        address: {

          home: action.address.home,
          city: action.address.city,
          state: action.address.state,
        },
      }

    default:
      return state
  }
}

export default addressReducer