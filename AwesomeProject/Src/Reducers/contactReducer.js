const contactReducer = (state = {
   
   contact: {

        id: 0,
        phoneNumber:8888141341,
      
    },
    contactForm: {
        id: 0,
        phoneNumber:0,
      
    }
}, action) => {

    switch (action.type) {
    
     

        case "ADD_CONTACT":
            
            return {
                ...state,
                contact: {
                    id:action.contact.id,
                    phoneNumber:action.contact.phoneNumber,
                },
                contactForm:{
                    id:action.contact.id,
                    phoneNumber:action.contact.phoneNumber,
                }
            }

        case "UPDATE_CONTACT":

            return {
                ...state,
                contact: {
                    id:action.contact.id,
                    phoneNumber:action.contact.phoneNumber,
                 
                },
                contactForm:{
                    id:action.contact.id,
                    phoneNumber:action.contact.phoneNumber,
                }
            }
        default:
            return state

    }
}
export default contactReducer