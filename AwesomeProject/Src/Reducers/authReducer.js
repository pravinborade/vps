const authReducer = (state = {
  user: {
    phone_number: ' ',
    firstName: '',
    lastName: '',
  },
  usersList: [
    
  ],
  token: null,
  loading: true,
  error: null,
},
  action) => {
  switch (action.type) {
    case "ADD_USER":
      return {
        ...state,
        user: {

          phone_number: action.user.phone_number,
          firstName: action.user.firstName,
          lastName: action.user.lastName,

        },
      }
    case "UPDATE_USER":
      return {
        ...state,

        user: {

          phone_number: action.user.phone_number,
          firstName: action.user.firstName,
          lastName: action.user.lastName,

        },
      }
    case "UPADTE_PASSWORD":
      return {
        ...state,
        user: {
          password: action.user.password,
          conformPassword: action.user.conformPassword,
        }
      }
    case "SAVE_USER":
      return {
        ...state,
        user: {
          phone_number: action.user.phone_number,
          firstName: action.user.firstName,
          lastName: action.user.lastName,
        }
      }

    case 'GET_TOKEN':
      return { ...state, token: action.token };
    case 'SAVE_TOKEN':
      return { ...state, token: action.token };
    case 'REMOVE_TOKEN':
      return { ...state, token: null };
    case 'LOADING':
      return { ...state, loading: action.isLoading };
    case 'ERROR':
      return { ...state, error: action.error };
    default:
      return state
  }
}

export default authReducer