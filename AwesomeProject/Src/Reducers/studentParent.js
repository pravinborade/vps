const  studentParentReducer=(state  = {
    
    parent:{
        
        
        firstName:'Vijay',
        middleName:'Vishwanath',
        lastName:'Nagre',
        guardian:'Vishwanath'

        
            
    },
    parentList:[]
},action)=>{

switch(action.type){

  case "ADD_PARENT":
      return{
          ...state,
          parent:{
            firstName:action.parent.firstName,
            middleName:action.parent.middleName,
            lastName:action.parent.lastName,
            guardian:action.parent.guardian,
          },
    }

    case "UPDATE_PARENT":
        
        return{
            ...state,
            parent:{
            firstName:action.parent.firstName,
            middleName:action.parent.middleName,
            lastName:action.parent.lastName,
            guardian:action.parent.guardian,
            
            }
        }
    default: 
        return state

}
}
export default  studentParentReducer