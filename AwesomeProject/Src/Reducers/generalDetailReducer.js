const generalDetailReducer = (state = {
   
    detail: {

        id: 0,
        admisionNumber:'S001',
        class: 'BE',
        batch: 'A',
        guardianName: 'V',
        admisionDate:new Date(),
    },
    generalDetailsForm: {
        id: 0,
        admisionNumber:0,
        class: '',
        batch: '',
        guardianName: '',
        admisionDate:new Date(),
    }
}, action) => {

    switch (action.type) {
    
     

        case "ADD_DETAIL":
            
            return {
                ...state,
               detail: {
                    id:action.detail.id,
                    class:action.detail.class,
                    admisionNumber:action.detail.admisionNumber,
                    batch:action.detail.batch,
                    guardianName:action.detail.guardianName,
                    admisionDate:new date (action.staff.admisionDate),

                },
                generalDetailsForm:{
                    id:action.detail.id,
                    class:action.detail.class,
                    admisionNumber:action.detail.admisionNumber,
                    batch:action.detail.batch,
                    guardianName:action.detail.guardianName,
                    admisionDate:new date (action.staff.admisionDate),
                }
            }

        case "UPDATE_DETAIL":

            return {
                ...state,
                detail: {
                    id:action.staff.id,
                    firstName: action.staff.firstName,
                    middleName: action.staff.middleName,
                    lastName: action.staff.lastName,
                    mobileNumber: action.staff.mobileNumber,
                    birthDate:new date (action.staff.birthDate),
                }
            }
        default:
            return state

    }
}
export default generalDetailReducer