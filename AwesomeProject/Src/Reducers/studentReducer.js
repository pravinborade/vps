const  studentReducer=(state  = {
    
    studentList:[
    
    ],


    student:{
        id:0,
        firstName:'Vijay',
        middleName:'V',
        lastName:'Nagre',
        birthDate:new Date(),
        gender:'MALE',
        bloodGroup:'A+',
        language:'MARATHI',
        
    },
    PersonalDetailForm:{
        id:0,
        firstName:'',
        middleName:'',
        lastName:'',
        birthDate:new Date(),
        gender:'',
        bloodGroup:'',
        language:'',
  
    },
    

},action)=>{

switch(action.type){

  case "ADD_STUDENT_LIST":
      return{
          ...state,
        studentList:action.students
    }

    case "ADD_STUDENT":
        return Object.assign({},state, {
            student:{
               id:action.student.id,
               firstName:action.student.firstName,
               middleName:action.student.middleName,
               lastName:action.student.lastName,
               birthDate:new Date(action.student.birthDate),
               gender:action.student.gender,
               bloodGroup:action.student.bloodGroup,
               language:action.student.language
               },
               PersonalDetailForm:{
                id:action.student.id,
                firstName:action.student.firstName,
                middleName:action.student.middleName,
                lastName:action.student.lastName,
                birthDate:new Date(action.student.birthDate),
                gender:action.student.gender,
                bloodGroup:action.student.bloodGroup,
                language:action.student.language
            }
        })

    default: 
        return state

}
}
export default studentReducer