const  feesReducer=(state  = {
   studFees:{
        totalFees:'7000',
        paidFees:'5000',
        pendingFees:'2000',
    },
  },
action)=> {
    switch (action.type) {
      case "ADD_FEES":
        return{
        ...state,
        studFees:{
        
        totalFees:action.studFees.totalFees,
        paidFees:action.studFees.paidFees,
        pendingFees:action.studFees.pendingFees
        },
    }
      case "UPDATE_FEES":
          return{
        ...state,
        studFees:{
            totalFees:action.studFees.totalFees,
            paidFees:action.studFees.paidFees,
            pendingFees:action.studFees.pendingFees
        },
    }
      
      case 'GET_TOKEN':
          return { ...state, token: action.token };
      case 'SAVE_TOKEN':
          return { ...state, token: action.token };
      case 'REMOVE_TOKEN':
          return { ...state, token: action.token };
      case 'LOADING':
          return { ...state, loading: action.isLoading };
      case 'ERROR':
          return { ...state, error: action.error };            
default:
        return state
    }
  }

  export default feesReducer