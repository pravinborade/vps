const staffReducer = (state = {
    staffList: [],
    staff: {

        id: 0,
        firstName: '',
        middleName: '',
        lastName: '',
        mobileNumber: '',
        birthDate:new Date(),
    },
    staffForm: {
        id: 0,
        firstName: '',
        middleName: '',
        lastName: '',
        mobileNumber: '',
        birthDate:new Date(),
    }
}, action) => {

    switch (action.type) {
    
        case "ADD_STAFF_LIST":
            debugger;
            return{
                ...state,
                staffList:action.staffs
            }

        case "ADD_STAFF":
            debugger
            return {
                ...state,
                staff: {
                    id:action.staff.id,
                    firstName: action.staff.firstName,
                    middleName: action.staff.middleName,
                    lastName: action.staff.lastName,
                    mobileNumber: action.staff.mobileNumber,
                    birthDate:new date (action.staff.birthDate),

                },
                staffForm:{
                    id:action.staff.id,
                    firstName:action.staff.firstName,
                    middleName:action.staff.middleName,
                    lastName:action.staff.lastName,
                    mobileNumber:action.staff.mobileNumber,
                    birthDate:new date (action.staff.birthDate),
                }
            }

        case "UPDATE_STAFF":

            return {
                ...state,
                staff: {
                    id:action.staff.id,
                    firstName: action.staff.firstName,
                    middleName: action.staff.middleName,
                    lastName: action.staff.lastName,
                    mobileNumber: action.staff.mobileNumber,
                    birthDate:new date (action.staff.birthDate),
                }
            }
        default:
            return state

    }
}
export default staffReducer