import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity
} from 'react-native';
import {
    Container, Header,
    Title, Left, Icon, Right,
    Button, Body, Content,
    Footer,
    FooterTab 
  } from "native-base";
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faArrowLeft,faCamera } from '@fortawesome/free-solid-svg-icons';
import {connect} from 'react-redux';

const AppHeader=(props)=> {
    const { title, navigation} = props
      return (
               
        <Header>
              <Left>
               <Button transparent onPress={() => navigation.goBack()} >
                  <FontAwesomeIcon icon={faArrowLeft} style={styles.icon} size={20} style={styles.IconColor}/>
            </Button>
              </Left>
              <Body>
                <Title>{title}</Title>
              </Body>
           <Right/>
          </Header>
      
    );
    
}
  const styles = StyleSheet.create({
    IconColor:{
        color:'#FFF',
      },
});

export default AppHeader;










    