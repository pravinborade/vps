import React from 'react';
import {reduxForm,Field,} from 'redux-form'
import {    
    View, Image, Text,
    TextInput,TouchableHighlight,StyleSheet
} from 'react-native';
import AwesomeButton from "react-native-really-awesome-button";
import OtpInputs from 'react-native-otp-inputs'



import {Item,Input, Form} from 'native-base';
import { TouchableOpacity } from 'react-native-gesture-handler';

const required = value => value ? undefined : 'Required'
const number = value => value && Number.isInteger(value) ? undefined  : 'Must be a number'; 
const phoneMinLength = value => value && value.length < 10 ? 'min 10 digit' : undefined;
const phoneMaxLength = value => value && value.length > 14 ? 'max 14 digit' : undefined;
const passMinLength = value => value && value.length < 6 ? 'min 6 character' : undefined;

let errors = {}
const validate = values => {
    if (!values.otp) {
        errors.otp = 'Required'
    } else if (values.otp.length < 6) {
        errors.otp = 'Enter 6 digit otp'
    } else if (values.otp.length >7) {
        errors.otp = 'Only 6 digit are allowed'
    }else if(isNaN(Number(values.otp))){
        errors.otp = 'must be  number'
    }
    return errors
  }



  

const renderInput = ({ input, label, type, keyboard, handleChange, meta: { touched, error, warning } }) => {
    var hasError= false;
    if(error !==undefined ){
        hasError= true;
    
    }else if(error){
        hasError = true;
      
    }
    console.log(errors);
    return( 
        
        <Item style={styles.inputs} error={hasError && touched}> 
          {/* <Input {...input}
                style={styles.inputs}
                placeholder={label}
                secureTextEntry={type=='password'}
                keyboardType={keyboard}
            />   
           */}
             <OtpInputs
                handleChange={(otp) => handleChange(otp)}
                numberOfInputs={6}
            />

        

            {/* {
                touched && ((error && <Text  style={styles.textDanger}>{error}</Text>)
                 || 
                (warning && <Text  style={styles.textDanger}>{warning}</Text>))
            } */}
        
        </Item>
    )
}

 let OtpForm =  (props) => {
    const { onSubmit, handleSubmit,submitting,pristine,handleChange,otp} = props
 
    return(
        <View noValidate> 
            <View style={{flex:12,flexDirection:'row'}}>
                <Field 
                    name="otp" 
                    component={renderInput} 
                    label="OTP" 
                    type="phone-pad"
                    keyboard="phone-pad"
                    handleChange={handleChange}
                    
                />
            </View>
            <View style={styles.btnAlign}>
                <TouchableHighlight 
                    onPress={ onSubmit(handleSubmit)}
                    style={styles.submitButton}
                     disabled={otp.length < 6}

                >
                    <Text style={styles.btnText}>
                        {
                            otp.length == 6
                                ?
                            'VERIFY AND CONTINUE'
                                :
                            'ENTER  OTP' 
                        }
                    </Text> 
                </TouchableHighlight>
            </View>
            
        </View>
    
      
    );
}


const styles = StyleSheet.create({
    btnText: {
        color: 'white',
        fontSize:20,
        textAlign:'center',
        paddingVertical:7
    },
    textDanger:{
        color:'red'
    },

    btnAlign:{
        justifyContent:'center',
        flex:12,
        flexDirection:'row'
    },

    inputs:{
     flex:1,
     alignSelf:'center',
     justifyContent:'center',
     alignItems:'center',

    },

    submitButton:{
        backgroundColor:'orange',
        color:'white',
        height:50,
        width:350
    }
    
  });
  OtpForm = reduxForm({
    form: 'otpForm',
    validate
  })(OtpForm)
  export default OtpForm