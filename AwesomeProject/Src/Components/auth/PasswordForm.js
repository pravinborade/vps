import React from 'react';
import {reduxForm,Field,} from 'redux-form'
import {    
    View, Image, Text,
    TextInput,TouchableHighlight,StyleSheet
} from 'react-native';
import AwesomeButton from "react-native-really-awesome-button";
import {Item,Input, Form} from 'native-base';
let errors = {}
const validate = values => {
    
    errors ={}

    if (!values.password) {
        errors.password = 'Required'
    } else if (values.password.length < 6) {
        errors.password = 'mus be 6 charecter '
    } return errors
  }


  const required = value => value ? undefined : 'Required'
  const passMinLength = value => value && value.length < 6 ? 'min 6 character' : undefined;



const renderInput = ({ input, label, type, keyboard, meta: { touched, error, warning } }) => {
    var hasError= false;
    if(error !==undefined ){
        hasError= true;
    
    }else if(error){
        hasError = true;
      
    }
    console.log(errors);
    return(
        <Item style={styles.formItem} error={hasError && touched}> 
            <Input {...input}
                 placeholderTextColor='rgb(0,0,0,0.5)'
               
                placeholder={label}
                secureTextEntry={type=='password'}
                keyboardType={keyboard}
            />
            {
                touched && ((error && <Text  style={styles.textDanger}>{error}</Text>)
                 || 
                (warning && <Text  style={styles.textDanger}>{warning}</Text>))
            }
        </Item>
    )
}

 let PasswordForm =  (props) => {
  
    const { onSubmit, handleSubmit,submitting,pristine,formError,phone} = props
   
    return(
        <View noValidate > 
                    {  
                        formError
                            ?
                        <Text style={styles.error}>password does not match</Text>
                            :
                        <Text></Text>
                        
                    }
                    <Field 
                        name="phone_number" 
                        component={renderInput} 
                        label="Mobile Number" 
                        type="phone-pad"
                        keyboard="phone-pad" 
                        value={ phone }
                        
                        
                      />
                    <Field
                        name="password" 
                        component={renderInput} 
                        label="New Password"
                        type="password"
                        validate={[ required,  passMinLength ]}
                      
                    /> 
                     <Field
                        name="conformPassword" 
                        component={renderInput} 
                        label=" Conform Password"
                        type="password"
                        validate={[ required,  passMinLength ]}
                      
                    /> 
                     
                    
            
          <View >
            <TouchableHighlight
                onPress={handleSubmit(onSubmit)}
                style={[styles.btnAlign,{backgroundColor:'orange',color:'white'}]}
            >
               
                <Text style={styles.btnText}>Save </Text> 
               
            </TouchableHighlight>
          </View>
        </View>
    );
}
const styles = StyleSheet.create({
    btnText: {
        color: 'white',
        fontSize:19,
        textAlign:'center',
        paddingTop:7

    },
    textDanger:{
        color:'red'
    },
    btnAlign:{
        marginTop:20,
        width:350,
        height:50,
    },
   
    error:{
        color:'red'
    }
  });
  PasswordForm = reduxForm({
    form: 'passwordForm',
    validate
  })(PasswordForm)
  
  export default  PasswordForm