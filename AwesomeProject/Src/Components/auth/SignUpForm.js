import React from 'react';
import {reduxForm,Field,} from 'redux-form'
import {    
    View, Image, Text,
    TextInput,TouchableHighlight,StyleSheet
} from 'react-native';
import AwesomeButton from "react-native-really-awesome-button";
import {Item,Input, Form} from 'native-base';
import { withNavigation } from 'react-navigation';
let errors = {}
const validate = values => {
    
    errors ={}

        if(!values.phone_number ){
            errors.phone_number='Required'
        }else if(values.phone_number<10){
            errors.phone_number='please enter 10 digit phone number'
        }else if(values.phone_number.length>14){
            errors.phone_number='max 14 digit are allowed'
        }else if(isNaN(Number(values.phone_number))){
            errors.phone_number = 'must be  number'
         }if(!values.password){
            errors.password='Required'
        }else if(values.password.length<6){
            errors.password='min 6 charecter are required'
        }if(!values.conformPassword){
            errors.conformPassword='Required'
        }else if(values.conformPassword.length<6){
            errors.conformPassword='min 6 charecter are required'
        }else if( values.conformPassword !==values.password){
            errors.conformPassword='password are not matched'
        }
         return errors
    }


  



const renderInput = ({ input, label, type, keyboard, meta: { touched, error, warning } }) => {
    var hasError= false;
    if(error !==undefined ){
        hasError= true;
    
    }else if(error){
        hasError = true;
      
    }
    console.log(errors);
    return(
        <Item style={styles.formItem} error={hasError && touched}> 
            <Input {...input}
             placeholderTextColor='rgb(0,0,0,0.5)'
                style={styles.inputs}
                placeholder={label}
                secureTextEntry={type=='password'}
                keyboardType={keyboard}
            />
            {
                touched && ((error && <Text  style={styles.textDanger}>{error}</Text>)
                 || 
                (warning && <Text  style={styles.textDanger}>{warning}</Text>))
            }
        </Item>
    )
}

 let SingUpForm =  (props) => {
    
    const { onSubmit, handleSubmit,formErrors } = props

    return(
        <View noValidate > 

                    {
                formErrors.map((value) =>{
                    return(
                    <Text style={styles.textDanger}>
                        {value}
                    </Text>   
                    );
                })

            }

            <Field 
                name="phone_number" 
                component={renderInput} 
                label="Mobile Number" 
                type="phone-pad"
                keyboard="phone-pad"
                
            />
              
            <Field
                name="password" 
                component={renderInput} 
                label="Password"
                type="password"
            /> 
            <Field
            name="conformPassword" 
            component={renderInput} 
            label="Conform Password"
            type="password"
            /> 
          <View >
            <TouchableHighlight
                onPress={handleSubmit(onSubmit)}
                style={[styles.btnAlign,{backgroundColor:'orange',color:'white'}]}
              
            >
            <Text style={styles.btnText} >SIGNUP </Text> 
            </TouchableHighlight>
          </View>
        </View>
    );
} 
const styles = StyleSheet.create({
    btnText: {
        color: 'white',
        fontSize:19,
        textAlign:'center',
        paddingTop:7

    },
    textDanger:{
        color:'red',
        
    },
    btnAlign:{
        marginTop:20,
        width:350,
        height:50,
    },
    
  });
  SingUpForm = reduxForm({
    form: 'singUpForm',
    validate
  })(SingUpForm)
  
  export default SingUpForm