import React from 'react';
import { reduxForm, Field, } from 'redux-form'
import {
    View, Image, Text,
    TextInput, TouchableHighlight, StyleSheet
} from 'react-native';

import { Item, Input, Form } from 'native-base';

let errors = {}
const validate = values => {

    errors = {}
    if (!values.phone_number) {
        errors.phone_number = 'Required'
    } else if (isNaN(Number(values.phone_number))) {
        errors.phone_number = 'must be number'
    } else if (values.phone_number < 10) {
        errors.phone_number = 'must be 10 digit are required'
    } else if (values.phone_number.length > 12) {
        errors.phone_number = 'max 12 digit are allowed'
    } if (!values.password) {
        errors.password = 'Required'
    } else if (values.password.length < 6) {
        errors.password = 'must be 6 charecter are required'
    }
    return errors
}


const renderInput = ({ input, label, type, keyboard, meta: { touched, error, warning } }) => {
    var hasError = false;
    if (error !== undefined) {
        hasError = true;

    } else if (error) {
        hasError = true;

    }

    return (
        <Item style={styles.formItem} error={hasError && touched}>
            <Input {...input}
                placeholderTextColor='rgb(0,0,0,0.5)'
                style={styles.inputs}
                placeholder={label}
                secureTextEntry={type == 'password'}
                keyboardType={keyboard}
            />
            {
                touched && ((error && <Text style={styles.textDanger}>{error}</Text>)
                    ||
                    (warning && <Text style={styles.textDanger}>{warning}</Text>))
            }
        </Item>
    )
}

let AuthForm = (props) => {

    const { formType, handleSubmit, onSubmit, formErrors } = props
    // if(Object.keys(formErrors).length>0){
    //     errors = formErrors;
    // }
    return (
        <View noValidate >
            {
                formErrors.map((value) => {
                    return (
                        <Text style={styles.textDanger}>
                            {value}
                        </Text>

                    );

                })

            }
            <Field
                name="phone_number"
                component={renderInput}
                label="Mobile Number"
                type="phone-pad"
                keyboard="phone-pad"
            />
            {formType != 'Reset' ?
                <Field
                    name="password"
                    component={renderInput}
                    label="Password"
                    type="password"

                />
                :
                <View></View>
            }

            <View >
                <TouchableHighlight
                    onPress={handleSubmit(onSubmit)}
                    style={[styles.btnAlign, { backgroundColor: 'orange', color: 'white' }]}
                >
                    <Text style={styles.btnText}>{formType} </Text>

                </TouchableHighlight>
            </View>
        </View>
    );
}
const styles = StyleSheet.create({
    btnText: {
        color: 'white',
        fontSize: 19,
        textAlign: 'center',
        paddingTop: 7,
    },
    textDanger: {
        color: 'red'
    },
    btnAlign: {
        marginTop: 20,
        width: 350,
        height: 50,
    },

});

AuthForm = reduxForm({
    form: 'authForm',
    validate
})(AuthForm)
export default AuthForm;