import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity
} from 'react-native';
import { faCamera, faChevronRight, faTimes } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { Right, Button,Icon} from 'native-base';
import {connect} from 'react-redux';




const ProfileImage=(props)=> {

  
       
   
      return (
  <View>
      <Image style={styles.avatar}  
      source={{uri: 'https://bootdey.com/img/Content/avatar/avatar6.png'}}/>
      <Text style={styles.name}>{props.user.name}</Text>           
      <View style={styles.profileContainer}>
      <FontAwesomeIcon icon={faChevronRight}  
          style={styles.icon}  
          size={30}
          onPress={()=>props.navigations.navigate('ListView')}
      />
  </View>
  </View>
                
    );
    
  }

  const styles = StyleSheet.create({
   
    avatar: {
      width: 100,
      height: 100,
      borderRadius: 63,
      borderWidth: 4,
      borderColor: "white",
      alignSelf:'center',
      
 
    },
    name:{
      fontSize:20,
      color:"#000",
      fontWeight:'600',
    },
    profileContainer:{
        width: 50,
        height: 50,
        borderWidth: 0,
        borderColor: "white",
        position:"absolute",
        bottom:40,
        right:0,
        left:140,
        alignItems:"center",
        justifyContent:'center'

    },
    icon:{
        color:'black',
        alignSelf:'center',
       
      },
  
    
});



const mapStateToProps=(state) =>{

  return {
    user:state.auth.user
  }
}


const mapDispatchToProps=(dispatch) =>{
 
  return {
  
  }
}

    export default connect(mapStateToProps, mapDispatchToProps) (ProfileImage)










    