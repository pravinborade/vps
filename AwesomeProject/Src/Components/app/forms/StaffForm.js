import React from 'react';
import { reduxForm, Field, } from 'redux-form'
import {
    View, Image, Text,
    TextInput, TouchableHighlight, StyleSheet

} from 'react-native';

import { connect } from 'react-redux';
import moment from 'moment';


import { Item, Input, Form, Textarea, DatePicker } from 'native-base';

const required = value => value ? undefined : 'Required'

let errors = {}

const validate = values => {

    errors = {}
    if (!values.firstName) {
        errors.firstName = 'Required'
    } else if (values.firstName.length > 15) {
        errors.firstName = 'max 15  charecter are allowed'
    } if (!values.middleName) {
        errors.middleName = 'Required'
    } else if (values.middleName.length > 15) {
        errors.middleName = 'max 15  charecter are allowed'
    } if (!values.lastName) {
        errors.lastName = 'Required'
    } else if (values.lastName.length > 15) {
        errors.lastName = 'max 15  charecter are allowed'
    } if (!values.mobileNumber) {
        errors.mobileNumber = 'Required'
    } else if (isNaN(Number(values.mobileNumber))) {
        errors.mobileNumber = 'must be number'
    } else if (values.mobileNumber < 10) {
        errors.mobileNumber = 'must be 10 digit are required'
    } else if (values.mobileNumber.length > 12) {
        errors.mobileNumber = 'max 12 digit are allowed'
    }

    return errors
}


const renderDatePicker = ({ input, label, type, keyboard, change, meta: { touched, error, warning } }) => {

    var hasError = false;
    if (error !== undefined) {
        hasError = true;
    } else if (error) {
        hasError = true;
    }

    return (
        <Item style={styles.formItem} error={hasError && touched}>
            <DatePicker {...input}
                placeholderTextColor='rgb(0,0,0,0.5)'
                locale={"en"}
                selected={input.value ? moment(input.value) : null}
                onDateChange={change}
                androidMode={"default"}
                placeholder="Birth Date"
            />

            {
                touched && ((error && <Text style={styles.textDanger}>{error}</Text>)
                    ||
                    (warning && <Text style={styles.textDanger}>{warning}</Text>))
            }


        </Item>
    )
};


const renderInput = ({ input, label, type, keyboard, meta: { touched, error, warning } }) => {
    var hasError = false;
    var touch = false;
    if (error !== undefined) {
        hasError = true;
    } else if (error) {
        hasError = true;
    }

    return (
        <Item style={styles.formItem} error={hasError && touched}>
            <Input {...input}
                placeholderTextColor='rgb(0,0,0,0.5)'
                style={styles.inputs} 
                placeholder={label}
                keyboardType={keyboard}
               
            />
            {
                touched && ((error && <Text style={styles.textDanger}>{error}</Text>)
                    ||
                    (warning && <Text style={styles.textDanger}>{warning}</Text>))
            }



        </Item>
    )
}

let StaffForm = (props) => {

     
    const { onSubmit, handleSubmit, onDateChange } = props

    return (
        <View noValidate  >

            <Field
                name="firstName"
                type="text"
                component={renderInput}
                label="First Name"

            />
            <Field
                name="middleName"
                type="text"
                component={renderInput}
                label="Middle Name"

            />
            <Field
                name="lastName"
                type="text"
                component={renderInput}
                label="Last Name"
            />

            <Field
                name="birthDate"
                component={renderDatePicker}
                label="Birth Date"
                type="date"
                keyboard="default"
                change={onDateChange}
            />


            <Field
                name="mobileNumber"
                component={renderInput}
                label="Mobile Number"
                type="phone-pad"
                keyboard="phone-pad"
            />

            <View style={styles.formItem}>
                <TouchableHighlight
                    onPress={handleSubmit(onSubmit)}
                    style={styles.submitButton}
                >
                    <Text style={styles.btnText}>Save</Text>
                </TouchableHighlight>
            </View>
        </View>


    );
}


const styles = StyleSheet.create({
    formItem: {
        width: 350,
        marginVertical: 10,

    },

    submitButton: {
        flex: 1,
        backgroundColor: "#22dd7f",
        justifyContent: 'center',
        alignItems: 'center',
        width: 300,
        height: 25,
        marginLeft: 40,
        marginTop: 10,
    },
    btnText: {
        color: 'white',
        fontSize: 19,

    },
    textDanger: {
        color: 'red'
    },
    btnPosition: {
        alignSelf: 'center',
        flex: 1
    },
    field: {
        backgroundColor: 'gray',

    },
    inputs: {


    },
    formItem: {
        color: 'green',
    }

});


StaffForm = reduxForm({
    form: 'staff-Form', 
    validate,
    enableReinitialize: true
})(StaffForm)



StaffForm = connect(
    
    state => ({
        
        initialValues: state.staff.staffForm
     }),
     
   )(StaffForm)






export default StaffForm