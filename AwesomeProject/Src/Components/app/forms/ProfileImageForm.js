import React, { Component } from 'react';
import {
  StyleSheet,
  Image,
  View,
  TouchableHighlight
 } from 'react-native';

import { faPencilAlt, faTimes, faArrowLeft, faCamera,faImages } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { Container, Header,  Left, Right, Content, Card, CardItem, Body, Text,  ActionSheet, Button,Icon } from 'native-base';
import { Navigations } from 'react-navigation';
import ImagePicker from 'react-native-image-picker';
import { TouchableOpacity } from 'react-native-gesture-handler';








openPanel = () => {
  ImagePicker.showImagePicker(options, (response) => {
    console.log('Response = ', response);
    debugger;
    if (response.didCancel) {
      console.log('User cancelled image picker');
    } else if (response.error) {
      console.log('ImagePicker Error: ', response.error);
    } else if (response.customButton) {
      console.log('User tapped custom button: ', response.customButton);
    } else {
      const source = { uri: response.uri };
  
      // You can also display the image using data:
      // const source = { uri: 'data:image/jpeg;base64,' + response.data };
  
      this.setState({
        avatarSource: source,
      });
    }
  });

}

export const  ProfileImageForm=( props )=> {
  
    const {openPanel, avatarSource} = props
   const {BUTTONS,DESTRUCTIVE_INDEX,CANCEL_INDEX} = props
return (
  <View >
    <Image style={styles.profileImage}  source={require('../../../Assets/Borade.png')}/>
    <Text style={styles.name}>{props.name}</Text>           
    <View style={styles.profileContainer}>
      <View  style={styles.icon}  >
    <FontAwesomeIcon icon={faCamera}  style={{color:'#FFF'}}  
         onPress={() => openPanel}
        onPress={() =>
            ActionSheet.show(
              {
                options: BUTTONS,
                cancelButtonIndex: CANCEL_INDEX,
                destructiveButtonIndex: DESTRUCTIVE_INDEX,
                title: "Profile Photo"
              },
              buttonIndex => {() => changeAction(buttonIndex)}
            )}
    />
        
  </View>
                           
  </View>
                    
</View>


);
}



  const styles = StyleSheet.create({
   
    imageContainer:{
        height:"100%",
        width:'100%',
        justifyContent:'center',
        alignItems:'center',
        marginVertical:'auto'
    },


   profileImage: {
        width: 170,
        height: 170,
        borderRadius: 73,
        borderWidth: 4,
        borderColor: "white",
        alignSelf:'center',
        paddingTop:30,
        
      },
    
    icon:{
        
        
        bottom:60,
        left:230,
        backgroundColor:'#008b8b',
        borderRadius:74,
        borderWidth:4,
        height:40,
        width:40,
        borderColor: "white",
        alignItems:'center',
        justifyContent:'center',
        color:'white',

      },

      name:{
        fontSize:20,
        color:"black",
        fontWeight:'600',
        textAlign:'center',
      },
      profilename:{
          color:'black',
          textAlign:'center'

      },
      headericon:{
          color:'white',
      },

      editText:{
            textAlign:'right',
            color:'#262626',
            right:30,
            bottom:180,
      },
  
});


export default ProfileImageForm