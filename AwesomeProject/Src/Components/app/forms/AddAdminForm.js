import React from 'react';
import {reduxForm,Field,} from 'redux-form'
import {    
    View, Image, Text,
    TextInput,TouchableHighlight,StyleSheet
} from 'react-native';

import {Item,Input, Form} from 'native-base';


const required = value => value ? undefined : 'Required'




let errors = {}
const validate = values => {
    
    errors ={}
   if(!values.phone_number){
       errors.phone_number='Required'
   }else if (isNaN(Number(values.phone_number))){
       errors.phone_number='must be number'
   }else if(values.phone_number<10){
      errors.phone_number='must be 10 digit are required' 
   }else if(values.phone_number.length>10){
       errors.phone_number='max 10 digit are allowed'
   }if(!values.firstName){
       errors.firstName='Required'
   }else if(values.firstName.length>15){
       errors.firstName='max 15  charecter are allowed'
   }if(!values.lastName){
    errors.lastName='Required'
   }else if(values.lastname.length>15){
    errors.lastname='max 15  charecter are allowed'
}    
    return errors
  }


  const renderInput = ({ input, label, type, keyboard, meta: { touched, error, warning } }) => {
    var hasError= false;
    if(error !==undefined ){
        hasError= true;
    
    }else if(error){
        hasError = true;
      
    }
    
    return(
        <Item style={styles.formItem} error={hasError && touched}> 
            <Input {...input}
                placeholderTextColor='rgb(0,0,0,0.5)'
                style={[styles.inputs,{width:350}]}
                placeholder={label} 
                secureTextEntry={type=='password'}
                keyboardType={keyboard}
            />
            {
                touched && ((error && <Text  style={styles.textDanger}>{error}</Text>)
                 || 
                (warning && <Text  style={styles.textDanger}>{warning}</Text>))
            }
        </Item>
    )
}

 let AddAdminForm =  (props) => {
    
    const { handleSubmit,onSubmit,formErrors} = props
    // if(Object.keys(formErrors).length>0){
    //     errors = formErrors;
    // }
    return(
        <View noValidate style={styles.container} > 
        {
            formErrors.map((value) =>{
                return(
                <Text style={styles.textDanger}>
                    {value}
                </Text>
            
                );
                
            })
                  
        }
            <Field
                name="firstname"
                type="text"
                component={renderInput}
                label="First Name"
                validate={[ required]}
            />
            <Field
                name="lastname"
                type="text"
                component={renderInput}
                label="Last Name"
                validate={[ required]}
            />
           
            <Field 
                name="phone_number" 
                component={renderInput} 
                label="Mobile Number" 
                type="phone-pad"
                keyboard="phone-pad"
            />
                      
        <View  >
            <TouchableHighlight
                onPress={handleSubmit(onSubmit)} 
                style={[styles.btnAlign,{backgroundColor:'orange',color:'white'}]}  
            >
            <Text  style={styles.btnText}>Create New Account </Text> 
               
            </TouchableHighlight>
        </View>
        </View>
    );
}
const styles = StyleSheet.create({
    container:{
      
      
    },
    btnText: {
        color: 'white',
        fontSize:19,
        textAlign:'center',
       
    },
    textDanger:{
        color:'red'
    },
    btnAlign:{
        flex:1,
       justifyContent:'center',
       alignItems:'center',
        width:350,
        height:50,
        marginTop:20,
        marginStart:30,
     },
     inputs:{
      paddingTop:10,
      textAlign:'center',
     }

    
});

AddAdminForm = reduxForm({
    form: 'addAdminForm',
    validate
})(AddAdminForm)
export default AddAdminForm;