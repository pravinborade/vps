import React from 'react';
import {reduxForm,Field,} from 'redux-form'
import {    
    View, Image, Text,
    TextInput,TouchableHighlight,StyleSheet
    
} from 'react-native';

import { connect } from 'react-redux';
import moment from 'moment';


import {Item,Input, Form,Textarea,DatePicker} from 'native-base';

const required = value => value ? undefined : 'Required'
const number = value => value && Number.isInteger(value) ? undefined  : 'Must be a number'; 
const phoneMinLength = value => value && value.length < 10 ? 'min 10 digit' : undefined;
const phoneMaxLength = value => value && value.length > 14 ? 'max 14 digit' : undefined;
const passMinLength = value => value && value.length < 6 ? 'min 6 character' : undefined;





const renderDatePicker = ({ input, label, type, keyboard, change, meta: { touched, error, warning } }) => {
 
    var hasError= false;
    if(error !== undefined){
        hasError= true;
    }else if(error){
        hasError = true;
    }

    return (
        <Item style={styles.formItem} error={hasError && touched}>           
            <DatePicker {...input} 
                locale={"en"}
                selected={input.value ? moment(input.value) : null} 
                onDateChange={change}
                androidMode={"default"}
                placeholder="Select Date"
            />
            {touched && error && <span>{error}</span>}
        </Item>
    )
};
  

const renderInput = ({ input, label, type, keyboard, meta: { touched, error, warning } }) => {
    var hasError= false;
    if(error !== undefined){
        hasError= true;
    }else if(error){
        hasError = true;
    }
   
    return(
        <Item style={styles.formItem} error={hasError && touched}> 
            <Input {...input}
                style={styles.inputs}ProfileinfoForm
                placeholder={label}
                keyboardType={keyboard}
                disabled={input.name == 'mobileNumber'}
            />
            {
                touched && ((error && <Text  style={styles.textDanger}>{error}</Text>)
                 || 
                (warning && <Text  style={styles.textDanger}>{warning}</Text>))
            }
        </Item>
    )
}

 let  ProfileForm =  (props) => {
     
     
    const {formType, onSubmit, handleSubmit, onDateChange} = props

    return(
        <View noValidate  > 
         
            <Field
            name="firstName"
            type="text"
            component={renderInput}
            label="First Name"
            validate={[ required]}
            />
             <Field
            name="middleName"
            type="text"
            component={renderInput}
            label="Middle Name"
            validate={[ required]}
            />
             <Field
            name="lastName"
            type="text"
            component={renderInput}
            label="Last Name"
            /> 
            

              <Field   
                name="dateOfBirth" 
                component={renderDatePicker} 
                label="dateOfBirth" 
                type="date"
                keyboard="default"
                validate={[ required ]}
                change={onDateChange}
            />  
            <Field 
                
                name="gender" 
                label="Gender" 
                user  type="text"
                component={renderInput} 
            />
              <Field
            name="relision"
            type="text"
            component={renderInput}
            label="Relision"
            /> 
              <Field
            name="cast"
            type="text"
            component={renderInput}
            label="Cast"
            /> 
            <Field
            name="subCast"
            type="text"
            component={renderInput}
            label="Sub Cast"
            />
             <Field
            name="nationality"
            type="text"
            component={renderInput}
            label="Nationality"
            />
 
            <View style={styles.formItem}>
                <TouchableHighlight 
                    onPress={handleSubmit(onSubmit)} 
                    style={[styles.buttonContainer, styles.submitButton,styles.btnPosition]} 
                >
                <Text style={styles.btnText}>Save{formType}</Text>
                </TouchableHighlight>
            </View>
        </View>
    
      
    );
}


const styles = StyleSheet.create({
    formItem :{
        width:350,
        marginVertical:10,
        
    },
    

  
    buttonContainer: {
      height:45,
      justifyContent: 'center',
      alignItems: 'center',
      marginBottom:20,
      width:250,
      borderRadius:30,
    },
    submitButton: {
      backgroundColor: "#0070c0",
      width:300
    },
    btnText: {
        color: 'white',
        fontSize:19,

    },
    textDanger:{
        color:'red'
    },
    btnPosition:{
        
        
         alignSelf:'center',
         bottom:0,
         top:0,
         left:25,
         flex:1

    },
    field:{
        backgroundColor:'gray',

    }

  });


  ProfileForm = reduxForm({
    form: 'profileForm' // a unique identifier for this form
  })(ProfileForm)
  

//   ProfileForm = connect(
//     state => ({
//       initialValues: state.student.st,
//     }),
//     // { load: loadAccount } // bind account loading action creator
//   )(ProfileForm)
  



  
  
  export default  ProfileForm