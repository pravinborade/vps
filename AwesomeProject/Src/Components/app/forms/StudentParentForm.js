import React from 'react';
import {reduxForm,Field,} from 'redux-form'
import {    
    View, Image, Text,
    TextInput,TouchableHighlight,StyleSheet
    
} from 'react-native';

import { connect } from 'react-redux';
import moment from 'moment';


import {Item,Input, Form,Textarea,DatePicker} from 'native-base';

const required = value => value ? undefined : 'Required'





const renderDatePicker = ({ input, label, type, keyboard, change, meta: { touched, error, warning } }) => {
 
    var hasError= false;
    if(error !== undefined){
        hasError= true;
    }else if(error){
        hasError = true;
    }

  
};
  

const renderInput = ({ input, label, type, keyboard, meta: { touched, error, warning } }) => {
    var hasError= false;
    if(error !== undefined){
        hasError= true;
    }else if(error){
        hasError = true;
    }
   
    return(
        <Item style={styles.formItem} error={hasError && touched}> 
            <Input {...input}
                style={styles.inputs}ProfileinfoForm
                placeholder={label}
                keyboardType={keyboard}
                disabled={input.name == 'mobileNumber'}
            />
            {
                touched && ((error && <Text  style={styles.textDanger}>{error}</Text>)
                 || 
                (warning && <Text  style={styles.textDanger}>{warning}</Text>))
            }
        </Item>
    )
}

 let  StudentParentForm =  (props) => {
     
     
    const { onSubmit, handleSubmit, onDateChange,formType} = props

    return(
        <View noValidate  > 
         
            <Field
            name="firstName"
            type="text"
            component={renderInput}
            label="First Name"
            validate={[ required]}
            />
             <Field
            name="middleName"
            type="text"
            component={renderInput}
            label="Middle Name"
            validate={[ required]}
            />
             <Field
            name="lastName"
            type="text"
            component={renderInput}
            label="Last Name"
            />
              <Field
            name="guardian"
            type="text"
            component={renderInput}
            label="Guardian"
            /> 
         
           
            <View style={styles.formItem}>
                <TouchableHighlight 
                    onPress={handleSubmit(onSubmit)} 
                    style={[styles.buttonContainer, styles.submitButton,styles.btnPosition]} 
                >
                <Text style={styles.btnText}>Save{formType}</Text>
                </TouchableHighlight>
            </View>
        </View>
    
      
    );
}


const styles = StyleSheet.create({
    formItem :{
        width:350,
        marginVertical:10,
        
    },
    

  
    buttonContainer: {
      height:45,
      justifyContent: 'center',
      alignItems: 'center',
      marginBottom:20,
      width:250,
      borderRadius:30,
    },
    submitButton: {
      backgroundColor: "#0070c0",
      width:300
    },
    btnText: {
        color: 'white',
        fontSize:19,

    },
    textDanger:{
        color:'red'
    },
    btnPosition:{
        
        
         alignSelf:'center',
         bottom:0,
         top:0,
         left:25,
         flex:1

    },
    field:{
        backgroundColor:'gray',

    },
    inputs:{
        borderTopColor:'black',
        borderBottomWidth:1, 
    }

  });


  StudentParentForm = reduxForm({
    form: 'studentParent' // a unique identifier for this form
  })(StudentParentForm)
  

//   ProfileForm = connect(
//     state => ({
//       initialValues: state.student.st,
//     }),
//     // { load: loadAccount } // bind account loading action creator
//   )(ProfileForm)
  



  
  
  export default  StudentParentForm