import React from 'react';
import {View, StyleSheet,TouchableHighlight,Text} from 'react-native';
import {reduxForm,Field} from 'redux-form'
import {connect} from 'react-redux';
import {Item,Input, Form,Textarea,Icon} from 'native-base';
import { CheckBox } from 'react-native-elements'


const renderInput = ({ input, label, type, keyboard, meta: { touched, error, warning } }) => {
          var hasError= false;
          if(error !== undefined){
              hasError= true;
          }else if(error){
              hasError = true;
    }


    return(
      <Item style={styles.formItem} error={hasError && touched}> 
                <Input {...input}
                        style={styles.inputs}AddressForm
                        placeholder={label}
                      
                        keyboardType={keyboard}
                />
                  {
                      touched && ((error && <Text  style={styles.textDanger}>{error}</Text>)
                      || 
                      (warning && <Text  style={styles.textDanger}>{warning}</Text>))
                  }
      </Item>
  )


  }

let  AddressForm =  (props) => {
  
     
    const {formType, onSubmit, handleSubmit,initialValues} = props

    return(
        <View noValidate  > 




              <CheckBox
                title='Click Here'
                checked={this.state.checked}
              />


            
            <Field
              name="addressLine1"
              type="text"
              label="AddressLine1"
              component={renderInput} 
            />
            <Field
              name="addressLine2"
              type="text"
              label="AddressLine2"
              component={renderInput} 
            />
            <Field 
              name="city"
              type="text"
              label="City"
              component={renderInput} 
              
            />
            <Field
              name="state"
              type="text"
              label="State"
              component={renderInput} 
            />
            <Field 
              name="pinCode"
              type="number"
              label="Pin Code"
              keyboard="phone-pad"
              component={renderInput} 
            />


            <View style={styles.formItem}>
                      <TouchableHighlight 
                          onPress={handleSubmit(onSubmit)} 
                          style={[styles.buttonContainer, styles.submitButton,styles.btnPosition]} 
                      >
                      <Text style={styles.btnText}>Save{formType}</Text>
                      </TouchableHighlight>
            </View>
        </View>
    );
}



const styles =  StyleSheet.create({




  formItem :{
              width:350,
              marginVertical:10,
              
            },

buttonContainer: {
                    height:45,
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginBottom:20,
                    width:250,
                    borderRadius:30,
                  },
submitButton: {
                backgroundColor: "#0070c0",
                width:300
              },
btnText: {
              color: 'white',
              fontSize:19,

          },
textDanger:{
              color:'red'
            },
btnPosition:{
              alignSelf:'center',
              bottom:0,
              top:0,
              left:25,
              flex:1

            },
field:{
        backgroundColor:'gray',

      }


})


AddressForm = reduxForm({
      form: 'addressForm' // a unique identifier for this form          
    })(AddressForm)
  

  AddressForm = connect(

          state => ({
            initialValues: state.address.address,

     
    }),
  
  )(AddressForm)
  
  export default  AddressForm