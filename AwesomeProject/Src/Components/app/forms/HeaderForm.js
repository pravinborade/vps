import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity
} from 'react-native';
import {
    Container, Header,
    Title, Left, Icon, Right,
    Button, Body, Content,
    Footer,
    FooterTab 
  } from "native-base";
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faArrowLeft,faCamera } from '@fortawesome/free-solid-svg-icons';
import {connect} from 'react-redux';

const HeaderForm=(props)=> {
    const { formType} = props
      return (
        <Container >        
        <Header>
              <Left>
               <Button transparent onPress={() => props.navigation.goBack()} >
                  <FontAwesomeIcon icon={faArrowLeft} style={styles.icon} size={20} style={styles.IconColor}/>
            </Button>
              </Left>
              <Body>
                <Title>{formType}</Title>
              </Body>
           <Right/>
          </Header>
      </Container>
    );
    
}
  const styles = StyleSheet.create({
    IconColor:{
        color:'#FFF',
      },
});

const mapStateToProps=(state) =>{

  return {
    
          }
}
const mapDispatchToProps=(dispatch) =>{
 
  return {
  
  }
}
export default connect(mapStateToProps, mapDispatchToProps) (HeaderForm)










    