import React from 'react';
import {reduxForm,Field,} from 'redux-form'
import {    
    View, Image, Text,
    TextInput,TouchableHighlight,StyleSheet
    
} from 'react-native';

import { connect } from 'react-redux';

import {Item,Input, Form,Textarea,Icon} from 'native-base';

const required = value => value ? undefined : 'Required'
const number = value => value && Number.isInteger(value) ? undefined  : 'Must be a number'; 
const phoneMinLength = value => value && value.length < 10 ? 'min 10 digit' : undefined;
const phoneMaxLength = value => value && value.length > 14 ? 'max 14 digit' : undefined;
const passMinLength = value => value && value.length < 6 ? 'min 6 character' : undefined;



const renderInput = ({ input, label, type, keyboard, meta: { touched, error, warning } }) => {
    var hasError= false;
    if(error !== undefined){
        hasError= true;
    }else if(error){
        hasError = true;
    }
   
    return(
        <Item style={styles.formItem} error={hasError && touched}> 
            <Input {...input}
                style={styles.inputs}ProfileinfoForm
                placeholder={label}
               
                keyboardType={keyboard}
               
            />
            {
                touched && ((error && <Text  style={styles.textDanger}>{error}</Text>)
                 || 
                (warning && <Text  style={styles.textDanger}>{warning}</Text>))
            }
        </Item>
    )
}

 let  ContactForm=  (props) => {
     
    const {formType, onSubmit, handleSubmit,initialValues} = props

    return(
        <View noValidate  > 


           

            <Field   
                name="mobileNumber" 
                component={renderInput} 
                label="Mobile Number" 
                type="number"
                keyboard="phone-pad"
                validate={[ required,  phoneMinLength ]}
                

            />

            <Field   
                name="secondaryContact" 
                component={renderInput} 
                label="Secondary Contact" 
                type="number"
                keyboard="phone-pad"
                validate={[ required,  phoneMinLength ]}
                

            />
           

          
            
            
          
            
 
            <View style={styles.formItem}>
                <TouchableHighlight 
                    onPress={handleSubmit(onSubmit)} 
                    style={[styles.buttonContainer, styles.submitButton,styles.btnPosition]} 
                >
                <Text style={styles.btnText}>Save{formType}</Text>
                </TouchableHighlight>
            </View>
        </View>
    
      
    );
}


const styles = StyleSheet.create({
    formItem :{
        width:350,
        marginVertical:10,
        
    },
    

  
    buttonContainer: {
      height:45,
      justifyContent: 'center',
      alignItems: 'center',
      marginBottom:20,
      width:250,
      borderRadius:30,
    },
    submitButton: {
      backgroundColor: "#0070c0",
      width:300
    },
    btnText: {
        color: 'white',
        fontSize:19,

    },
    textDanger:{
        color:'red'
    },
    btnPosition:{
        
        
         alignSelf:'center',
         bottom:0,
         top:0,
         left:25,
         flex:1

    },
    field:{
        backgroundColor:'gray',
    }

  });


  ContactForm = reduxForm({
    form: 'contactForm' 
  })(ContactForm)
  

//    ContactForm = connect(
//      state => ({
//       initialValues:state.contact.contact,

      
// //     }),
    
//   )(ContactForm)
  
  export default  ContactForm