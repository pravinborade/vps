import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableHighlight,
  Image,
  Alert
} from 'react-native';
import {
  Container, Header,
  Title, Left, Icon, Right,
  Button, Body, Content,
  Footer,
  FooterTab
} from "native-base";

import { connect } from 'react-redux';
import AddressForm from '../Student/AddressForm';
import axios from 'axios';
import { baseURL, jsonHeader } from '../../../Config';
import AppHeader from '../../AppHeader';


class GeneralDetailPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
     
      formEdit:false,
      formError: [],
    }

   
  }
  render() {


    return (
      <Container>
        <AppHeader title={"Address Detail"} navigation={this.props.navigation} />
         
        <Content >
          <View>
            <AddressForm onSubmit={this.submit} formErrors={this.state.formError}  />
          </View>
        </Content>
      </Container>
    );
  }
}


const styles = StyleSheet.create({



  title: {
    fontSize: 20,
    color: "#000",
    paddingTop: 0
  },
  form: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-around',
    justifyContent: 'center',
    alignItems: 'center'

  }


})

const mapStateToProps = (state) => {
  
  return {
    
  }
}


const mapDispatchToProps = (dispatch) => {
  
  return {

    
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(GeneralDetailPage)