import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableHighlight,
  Image,
  Alert
} from 'react-native';
import {
  Container, Header,
  Title, Left, Icon, Right,
  Button, Body, Content,
  Footer,
  FooterTab, Accordion, List, ListItem,Separator
} from "native-base";

import { connect } from 'react-redux';
import axios from 'axios';
import { baseURL, jsonHeader } from '../../../Config';
import AppHeader from '../../AppHeader';
import ProfileImageForm from '../../../Components/app/forms/ProfileImageForm'


var BUTTONS = [
  { text: "Gallery", icon: "image", iconColor: "#2c8ef4" },
  { text: "Camera", icon: "camera", iconColor: "#f42ced" },
  { text: "Delete", icon: "trash", iconColor: "#fa213b" },
  { text: "Cancel", icon: "close", iconColor: "#25de5b" }
];
var DESTRUCTIVE_INDEX = 1;
var CANCEL_INDEX = 3;


class StudentProfilePage extends Component {
  constructor(props) {
    super(props);
    this.state = {

      formEdit: false,
      formError: [],
    }


  }

  render() {


    return (
      <Container>
        <AppHeader title={"Profile"} navigation={this.props.navigation} />

        <Content >
          <View>
            <ProfileImageForm BUTTONS={BUTTONS} DESTRUCTIVE_INDEX={DESTRUCTIVE_INDEX} CANCEL_INDEX={CANCEL_INDEX} />
            <Text style={styles.studentName}>
              {this.props.student.firstName}  {this.props.student.lastName} 
          </Text>
          <Text style={styles.border}></Text>
          </View>
          <View>
        
          </View>

          <View>


    <View>
          <List>
          <Separator bordered style={styles.seperator}>
            <Text style={styles.titleColor}>General Detail</Text>
              </Separator>
              <ListItem style={[styles.itemHeight,{marginTop:10}]}>
                  <Left>
                  <View>
                  <Text style={styles.itemHeader}>Admision Number</Text>
                  </View>
                  </Left> 
                  <Body>
                  <View>
                    <Text style={styles.textCenter}> {this.props.detail.admisionNumber}</Text>
                  </View>
                  </Body>
                  <Right />
              </ListItem>

              <ListItem style={styles.itemBackgroundColor} >
                  <Left>
                  <View>
                  <Text style={styles.itemHeader}>Class</Text>
                  </View>
                  </Left> 
                  <Body>
                  <View>
                    <Text style={styles.textCenter}> {this.props.detail.class}</Text>
                  </View>
                  </Body>
                  <Right />
              </ListItem>
              <ListItem style={styles.itemHeight} >
                  <Left>
                  <View>
                  <Text style={styles.itemHeader}> Birth Date</Text>
                  </View>
                  </Left> 
                  <Body>
                  <View>
                    <Text style={styles.textCenter}> {this.props.detail.birthDate}</Text>
                  </View>
                  </Body>
                  <Right />
              </ListItem>
              <ListItem style={styles.itemBackgroundColor} >
                  <Left>
                  <View>
                  <Text style={styles.itemHeader}> Batch</Text>
                  </View>
                  </Left> 
                  <Body>
                  <View>
                    <Text style={styles.textCenter}> {this.props.detail.batch}</Text>
                  </View>
                  </Body>
                  <Right />
              </ListItem>
              <ListItem style={styles.itemHeight} >
                  <Left>
                  <View>
                  <Text style={styles.itemHeader}> Guardian Name</Text>
                  </View>
                  </Left> 
                  <Body>
                  <View>
                    <Text style={styles.textCenter}> {this.props.detail.guardianName}</Text>
                  </View>
                  </Body>
                  <Right />
              </ListItem>
              <ListItem style={styles.itemBackgroundColor} >
                  <Left>
                  <View>
                  <Text style={styles.itemHeader}> Admision Date</Text>
                  </View>
                  </Left> 
                  <Body>
                  <View>
                    {/* <Text style={styles.textCenter}> {this.props.detail.admisionDate}</Text> */}
                  </View>
                  </Body>
                  <Right />
              </ListItem>
              <Separator bordered style={styles.seperator}>
                <Text style={styles.titleColor}>Personal Details</Text>
              </Separator>
              <ListItem style={styles.itemHeight} >
                  <Left>
                  <View>
                  <Text style={styles.itemHeader}>Gender</Text>
                  </View>
                  </Left> 
                  <Body>
                  <View>
                    <Text style={styles.textCenter}> {this.props. student.gender}</Text>
                  </View>
                  </Body>
                  <Right />
              </ListItem>
              <ListItem style={styles.itemBackgroundColor} >
                  <Left>
                  <View>
                  <Text style={styles.itemHeader}>Birth Date</Text>
                  </View>
                  </Left> 
                  <Body>
                  <View>
                    {/* <Text style={styles.textCenter}> {this.props.student.birthDate}</Text> */}
                  </View>
                  </Body>
                  <Right />
              </ListItem>
              <ListItem style={styles.itemHeight} >
                  <Left>
                  <View>
                  <Text style={styles.itemHeader}>Blood Group</Text>
                  </View>
                  </Left> 
                  <Body>
                  <View>
                    <Text style={styles.textCenter}> {this.props.student.bloodGroup}</Text>
                  </View>
                  </Body>
                  <Right />
              </ListItem>
              <ListItem style={styles.itemBackgroundColor} >
                  <Left>
                  <View>
                  <Text style={styles.itemHeader}>Language</Text>
                  </View>
                  </Left> 
                  <Body>
                  <View>
                    <Text style={styles.textCenter}> {this.props.student.language}</Text>
                  </View>
                  </Body>
                  <Right />
              </ListItem>
              <Separator bordered style={styles.seperator}>
                <Text style={styles.titleColor}>Address</Text>
              </Separator>
              <ListItem style={styles.itemHeight} >
                  <Left>
                  <View>
                  <Text style={styles.itemHeader}>Home</Text>
                  </View>
                  </Left> 
                  <Body>
                  <View>
                    <Text style={styles.textCenter}> {this.props.address.home}</Text>
                  </View>
                  </Body>
                  <Right />
              </ListItem>
              <ListItem style={styles.itemBackgroundColor} >
                  <Left>
                  <View>
                  <Text style={styles.itemHeader}>City</Text>
                  </View>
                  </Left> 
                  <Body>
                  <View>
                    <Text style={styles.textCenter}> {this.props.address.city}</Text>
                  </View>
                  </Body>
                  <Right />
              </ListItem>
              <ListItem style={styles.itemHeight} >
                  <Left>
                  <View>
                  <Text style={styles.itemHeader}>State</Text>
                  </View>
                  </Left> 
                  <Body>
                  <View>
                    <Text style={styles.textCenter}> {this.props.address.state}</Text>
                  </View>
                  </Body>
                  <Right />
              </ListItem>
              <Separator bordered style={styles.seperator}>
                <Text style={styles.titleColor}>Contact</Text>
              </Separator>
              <ListItem style={styles.itemBackgroundColor} >
                  <Left>
                  <View>
                  <Text style={styles.itemHeader}>Contact</Text>
                  </View>
                  </Left> 
                  <Body>
                  <View>
                    <Text style={styles.textCenter}> {this.props.contact.phoneNumber}</Text>
                  </View>
                  </Body>
                  <Right />
              </ListItem>
              <View>
                <Text style={styles.border}></Text>
              </View>
              <View>
              <Text  style={styles.help} >Help</Text>
              </View>
              <Separator bordered >
                
               
              </Separator>
             
          </List>

        </View>
      </View>
    </Content>
  </Container>
    );
  }
}


const styles = StyleSheet.create({



  title: {
    fontSize: 20,
    color: "#000",
    paddingTop: 0
  },
  form: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-around',
    justifyContent: 'center',
    alignItems: 'center'

  },
  border: {
    borderRadius: 1, 
    borderWidth:0.3,
    borderColor: '#000',
  },
  textDanger: {
    color: '#169ae2'
  },
  itemHeight:{
    backgroundColor: 'whitesmoke',
    height:50,
    
    },
    itemBackgroundColor:{
      backgroundColor: '#FFF',
      height:50,
    },
    itemHeader:{
      color:'gray',
      fontSize:15,
      paddingLeft:10,

    },
   textCenter:{
    justifyContent:'center',
    alignItems:'center',
    color:'#000'
   },
   seperator: {
    height:50,
    paddingTop: 5,
    paddingBottom: 5
  },     
  titleColor:{
    color:'#007AFF',
    fontSize:20,
  },
  studentName:{
    justifyContent:'center',
   
    fontSize:20,
    color:'#000',
    marginBottom:40,
    textAlign:'center'
    
  },
  helpBox:{
    height:400,
    color:'gray',
  },
  help:{
    color:'#333333',
    fontSize:10
  }

     


})

const mapStateToProps = (state) => {

  return {
    student: state.student.student,
    detail: state.generalDetail.detail,
    address:state.address.address,
    contact:state.contact.contact,

  }
}


const mapDispatchToProps = (dispatch) => {

  return {

  }
}
export default connect(mapStateToProps, mapDispatchToProps)(StudentProfilePage)