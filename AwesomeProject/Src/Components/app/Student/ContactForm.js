import React from 'react';
import { Container, Header, Content, Card, CardItem, Body } from "native-base";

import { reduxForm, Field, } from 'redux-form'
import {
  View, Image, Text,
  TextInput, TouchableHighlight, StyleSheet

} from 'react-native';

import { connect } from 'react-redux';
import moment from 'moment';


import { Item, Input, Form, Textarea, DatePicker } from 'native-base';

const required = value => value ? undefined : 'Required'

let errors = {}

const validate = values => {

  errors = {}
  if (!values.phoneNumber) {
    errors.phoneNumber = 'Required'
  } else if (isNaN(Number(values.phoneNumber))) {
    errors.phoneNumber = 'must be number'
  } else if (values.phoneNumber < 10) {
    errors.phoneNumber = 'must be 10 digit are required'
  } else if (values.phoneNumber.length > 12) {
    errors.phoneNumber = 'max 12 digit are allowed'
  }
  return errors
}


const renderDatePicker = ({ input, label, type, keyboard, change, meta: { touched, error, warning } }) => {

  var hasError = false;
  if (error !== undefined) {
    hasError = true;
  } else if (error) {
    hasError = true;
  }

  return (
    <Item style={styles.formItem} error={hasError && touched}>
      <DatePicker {...input}
        locale={"en"}
        selected={input.value ? moment(input.value) : null}
        onDateChange={change}
        androidMode={"default"}
        placeholder="Birth Date"
       
      />

      {
        touched && ((error && <Text style={styles.textDanger}>{error}</Text>)
          ||
          (warning && <Text style={styles.textDanger}>{warning}</Text>))
      }


    </Item>
  )
};


const renderInput = ({ input, label, type, keyboard, meta: { touched, error, warning } }) => {
  var hasError = false;
  var touch = false;
  if (error !== undefined) {
    hasError = true;
  } else if (error) {
    hasError = true;
  }

  return (
    <Item  floatingLabel style={styles.formItem} error={hasError && touched}>
       <Label style={styles.fieldLablel}>{label}</Label>
      <Input {...input}
       
        style={styles.inputs}
        // placeholder={label}
        keyboardType={keyboard}
        
      />
      {
        touched && ((error && <Text style={styles.textDanger}>{error}</Text>)
          ||
          (warning && <Text style={styles.textDanger}>{warning}</Text>))
      }



    </Item>
  )
}

let ContactForm = (props) => {


  const { onSubmit, handleSubmit, onDateChange } = props

  return (

    <Container>
 
      <Content padder>
        <Card>
          <CardItem header bordered>
            <Text style={styles.formHeader}>Personal Detail</Text>
          </CardItem>
          <CardItem bordered>
            <Body>

              <Field
                name="phoneNumber"
                component={renderInput}
                label="Phone Number"
                type="phone-pad"
                keyboard="phone-pad"
              />
          
            </Body>
          </CardItem>

          <View >
              <TouchableHighlight onPress={handleSubmit(onSubmit)} style={styles.submitButton}>
                <Text style={styles.btnText}>Save</Text>
              </TouchableHighlight>

          </View>

        </Card>

      </Content>

    </Container>


  );
}


const styles = StyleSheet.create({
  formItem: {
    width: 300,
    marginVertical: 10,

  },

  submitButton: {
    flex: 1,
    backgroundColor: "#22dd7f",
    justifyContent: 'center',
    alignItems: 'center',
    width: 300,
    height: 25,
    marginTop:10,
    marginLeft:35,
    marginBottom:10,
  },
  btnText: {
    color: 'white',
    fontSize: 19,

  },
  textDanger: {
    color: 'red'
  },
  btnPosition: {
    alignSelf: 'center',
    flex: 1
  },
  field: {
    flex: 1,
    flexDirection: 'row',
    width: 200,
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center'
  },
  placeholder: {
    flex: 1,
    flexDirection: 'column',
    height: 50,
    justifyContent: 'center',
  },
  formHeader:{
    color:'#141823',
    fontSize:21,

  },
  inputs: {


  },
  formItem: {
    color: 'green',
  },
  fieldLablel:{
    paddingLeft:5
  }

});


ContactForm = reduxForm({
  form: 'contactForm', // a unique identifier for this form
  validate,
  enableReinitialize: true

})(ContactForm)


ContactForm = connect(
  state => ({
    initialValues: state.contact.contactForm,
  }),
)(ContactForm)






export default ContactForm