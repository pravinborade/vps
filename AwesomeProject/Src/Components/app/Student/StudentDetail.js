import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableHighlight,
  Image,
  Alert
} from 'react-native';
import {
  Container, Header,
  Title, Left, Icon, Right,
  Button, Body, Content,
  Footer,
  FooterTab
} from "native-base";

import { connect } from 'react-redux';
import PersonalDetailForm from '../Student/PersonalDetailForm';
import axios from 'axios';
import { baseURL, jsonHeader } from '../../../Config';
import AppHeader from '../../AppHeader';
import {addStudent} from '../../../Actions/studentActions';


class StudentDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
     
      formEdit:false,
      formError: [],
    }

   
  }

  onDateChange =  (date) => {
    this.props.input.onDateChange(moment(date));
  }

  getStudent(){
    var id=this.props.navigation.state.params.Id
   
    if(id>0){
      axios.get(`${baseURL}/api/student/`+id, jsonHeader)
      .then((result) => {
          this.props.addStudent(result.data);
        }).catch((error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      )
    }else{
        this.setState({formEdit:true}) 
      }
  }
  
  
  componentDidMount(){
  
  } 


  submit = values => {

   

  }
   // values.birthDate=values.birthDate.toString();
  //   const formData = new FormData()
  //   formData.append('id',values.id)
  //   formData.append('firstName',values.firstName)
  //   formData.append('middleName',values.middleName)
  //   formData.append('lastName',values.lastName)
  //   formData.append('birthDate',values.birthDate)


    
  //   return axios.post(`${baseURL}/api/student/add`, formData, jsonHeader)
  //     .then((response) => {
         
  //       this.props.addStudent(response.data)

  //     })
  //     .catch((error) => {
        
       
  //     })
  //     .finally(function () {

  //     });
  // }


 

  render() {


    return (
      <Container>
        <AppHeader title={"Student Detail"} navigation={this.props.navigation} />
         
        <Content >
          <View>
            <PersonalDetailForm onSubmit={this.submit} formErrors={this.state.formError}  />
          </View>
        </Content>
      </Container>
    );
  }
}


const styles = StyleSheet.create({



  title: {
    fontSize: 20,
    color: "#000",
    paddingTop: 0
  },
  form: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-around',
    justifyContent: 'center',
    alignItems: 'center'

  }


})

const mapStateToProps = (state) => {
  
  return {
    student:state.student.student
  }
}


const mapDispatchToProps = (dispatch) => {
  
  return {

    addStudent:(data) => dispatch(addStudent(data)),
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(StudentDetail)