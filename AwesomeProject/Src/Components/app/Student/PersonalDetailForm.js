import React from 'react';
import { Container, Header, Content, Card, CardItem, Body, Label } from "native-base";
import { reduxForm, Field, } from 'redux-form'
import {
  View, Image, Text,
  TextInput, TouchableHighlight, StyleSheet

} from 'react-native';

import { connect } from 'react-redux';
import moment from 'moment';


import { Item, Input, Form, Textarea, DatePicker } from 'native-base';

const required = value => value ? undefined : 'Required'

let errors = {}

const validate = values => {
  
  errors = {}
  if (!values.firstName) {
    errors.firstName = 'Required'
  } else if (values.firstName.length > 15) {
    errors.firstName = 'max 15  charecter are allowed'
  } if (!values.middleName) {
    errors.middleName = 'Required'
  } else if (values.middleName.length > 15) {
    errors.middleName = 'max 15  charecter are allowed'
  } if (!values.lastName) {
    errors.lastName = 'Required'
  } else if (values.lastName.length > 15) {
    errors.lastName = 'max 15  charecter are allowed'
  } if (!values.bloodGroup) {
    errors.bloodGroup = 'Required'
  } else if (values.bloodGroup > 15) {
    errors.bloodGroup = 'max 15  charecter are allowed'
  } if (!values.language) {
    errors.language = 'Rquired'
  } else if (values.language > 15) {
    errors.language = 'max 15  charecter are allowed'
  } if (!values.gender) {
    errors.gender = 'Required'
  } else if (values.gender > 5) {
    errors.gender = 'max 5  charecter are allowed'
  }if(!values.birthDate){
  
    errors.birthDate='Required'
  }
  
  return errors
}


const renderDatePicker = ({ input, label, type, keyboard, change, meta: { touched, error, warning } }) => {

  var hasError = false;
  if (error !== undefined) {
    hasError = true;
  } else if (error) {
    hasError = true;
  }

  return (
    <Item style={styles.formItem} error={hasError && touched}>
      {/* <DatePicker {...input}
        locale={"en"}
        selected={input.value ? moment(input.value) : null}
        onDateChange={change}
        androidMode={"default"}
        placeholder="Birth Date"
       
      /> */}
      

      <DatePicker {...input}
            defaultDate={new Date(2018, 4, 4)}
            minimumDate={new Date(2018, 1, 1)}
            maximumDate={new Date(2018, 12, 31)}
            locale={"en"}
            timeZoneOffsetInMinutes={undefined}
            modalTransparent={false}
            animationType={"fade"}
            androidMode={"default"}
            placeHolderText="Birth Date"
            textStyle={{ color: "#000" }}
            placeHolderTextStyle={{ color: "gray" }}
            onDateChange={change}
            disabled={false}
            />
      {
        touched && ((error && <Text style={styles.textDanger}>{error}</Text>)
          ||
          (warning && <Text style={styles.textDanger}>{warning}</Text>))
      }


    </Item>
  )
};


const renderInput = ({ input, label, type, keyboard, meta: { touched, error, warning } }) => {
  var hasError = false;
  var touch = false;
  if (error !== undefined) {
    hasError = true;
  } else if (error) {
    hasError = true;
  }

  return (
    <Item  floatingLabel   style={styles.formItem} error={hasError && touched} >
      <Label style={styles.fieldLablel}>{label}</Label>
      <Input {...input}
        style={styles.inputs}
        // placeholder={label}
        keyboardType={keyboard}
        
      />
      {
        touched && ((error && <Text style={styles.textDanger}>{error}</Text>)
          ||
          (warning && <Text style={styles.textDanger}>{warning}</Text>))
      }



    </Item>
  )
}

let PersonalDetailForm = (props) => {


  const { onSubmit, handleSubmit, onDateChange } = props

  return (


    <Container>
      
      <Content padder>
        <Card>
          <CardItem header bordered>
            <Text style={styles.formHeader}>Personal Detail</Text>
          </CardItem>
          {/* <CardItem bordered> */}
            <Body>

            
           
              <Field
                name="firstName"
                type="text"
                component={renderInput}
                label="First Name"
               
              />
             
              <Field
                name="middleName"
                type="text"
                component={renderInput}
                label="Middle Name"
               
              />

              <Field
                name="lastName"
                type="text"
                component={renderInput}
                label="Last Name"
              />

              {/* <Field
                name="birthDate"
                component={renderDatePicker}
                label="Birth Date"
                type="date"
                keyboard="default"
                change={onDateChange}
               
              /> */}

              <Field

                name="gender"
                label="Gender"
                user type="text"
                component={renderInput}
              />
              <Field
                name="bloodGroup"
                type="text"
                component={renderInput}
                label="Blood Group"
              />

              <Field
                name="language"
                type="text"
                component={renderInput}
                label="Language"
              />
            
            
            </Body>
        
          {/* </CardItem> */}
          <View>
              <TouchableHighlight onPress={handleSubmit(onSubmit)} style={styles.submitButton}>
                <Text style={styles.btnText}>Save</Text>
              </TouchableHighlight>

            </View>
        </Card>

      </Content>

    </Container>
  );
}


const styles = StyleSheet.create({
  formItem: {
    width:350,
    marginVertical: 10,
    paddingLeft:30,

  },

  submitButton: {
    flex: 1,
    backgroundColor: "#22dd7f",
    justifyContent: 'center',
    alignItems: 'center',
    width: 300,
    height: 40,
    marginTop:10,
    marginLeft:35,
    marginBottom:10,
  },
  btnText: {
    color:'white',
    fontSize: 19,

  },
  textDanger: {
    color: 'red'
  },
  btnPosition: {
   paddingVertical:10,
  },
  field: {
    flex: 1,
    flexDirection: 'row',
    width: 200,
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center'
  },
  placeholder: {
    flex: 1,
    flexDirection: 'column',
    height: 50,
    justifyContent: 'center',
  },
  formHeader:{
    color:'#141823',
    fontSize:21,

  },
  inputs: {


  },
  fieldLablel:{
    paddingLeft:5
  }


});


PersonalDetailForm = reduxForm({
  form: 'personalDetailForm', // a unique identifier for this form
  validate,
  enableReinitialize: true

})(PersonalDetailForm)


PersonalDetailForm = connect(
  state => ({
    initialValues: state.student.studentForm,
  }),
)(PersonalDetailForm)






export default PersonalDetailForm