import React from 'react';
import { Container, Header, Content, Card, CardItem, Body } from "native-base";

import { reduxForm, Field, } from 'redux-form'
import {
  View, Image, Text,
  TextInput, TouchableHighlight, StyleSheet

} from 'react-native';

import { connect } from 'react-redux';
import moment from 'moment';


import { Item, Input, Form, Textarea, DatePicker } from 'native-base';

const required = value => value ? undefined : 'Required'

let errors = {}

const validate = values => {

  errors = {}
  if (!values.class) {
    errors.class = 'Required'
  } else if (values.class.length > 15) {
    errors.class = 'max 15  charecter are allowed'
  } if (!values.batch) {
    errors.batch = 'Required'
  } else if (values.batch.length > 15) {
    errors.batch = 'max 15  charecter are allowed'
  } if (!values.guardianName) {
    errors.guardianName = 'Required'
  } else if (values.guardianName.length > 15) {
    errors.guardianName = 'max 15  charecter are allowed'
  } if (!values.admisionNumber) {
    errors.admisionNumber = 'Required'
  } else if (values.admisionNumber.length > 15) {
    errors.admisionNumber = 'max 15  charecter are allowed'
  }if (!values.admisionDate){
    errors.admisionDate='Required'
  }else if(values.admisionDate==null || values.admisionDate==''){
    errors.admisionDate='Date must be reqired'
  }
  return errors
}


const renderDatePicker = ({ input, label, type, keyboard, change, meta: { touched, error, warning } }) => {

  var hasError = false;
  if (error !== undefined) {
    hasError = true;
  } else if (error) {
    hasError = true;
  }

  return (
    <Item style={styles.formItem} error={hasError && touched}>
      {/* <DatePicker {...input}
        locale={"en"}
        selected={input.value ? moment(input.value) : null}
        onDateChange={change}
        androidMode={"default"}
        placeholder="Birth Date"
      /> */}

          <DatePicker {...input}
            defaultDate={new Date(2018, 4, 4)}
            minimumDate={new Date(2018, 1, 1)}
            maximumDate={new Date(2018, 12, 31)}
            locale={"en"}
            timeZoneOffsetInMinutes={undefined}
            modalTransparent={false}
            animationType={"fade"}
            androidMode={"default"}
            placeHolderText="Admision Date"
            textStyle={{ color: "green" }}
            placeHolderTextStyle={{ color: "gray" }}
            onDateChange={this.setDate}
            disabled={false}
            />

      {
        touched && ((error && <Text style={styles.textDanger}>{error}</Text>)
          ||
          (warning && <Text style={styles.textDanger}>{warning}</Text>))
      }


    </Item>
  )
};


const renderInput = ({ input, label, type, keyboard, meta: { touched, error, warning } }) => {
  var hasError = false;
  var touch = false;
  if (error !== undefined) {
    hasError = true;
  } else if (error) {
    hasError = true;
  }

  return (
    <Item  floatingLabel  style={styles.formItem} error={hasError && touched}>
      <Label style={styles.fieldLablel}>{label}</Label>
      <Input {...input}

        style={styles.inputs}
        keyboardType={keyboard}
      
      />
      {
        touched && ((error && <Text style={styles.textDanger}>{error}</Text>)
          ||
          (warning && <Text style={styles.textDanger}>{warning}</Text>))
      }



    </Item>
  )
}

let GeneralDetailsForm = (props) => {


  const { onSubmit, handleSubmit, onDateChange } = props

  return (


    <Container>
   
      <Content padder>
        <Card>
          <CardItem header bordered>
            <Text style={styles.formHeader}>General Detail</Text>
          </CardItem>
          <CardItem bordered>
            <Body>

              <Field
                name="admisionNumber"
                type="text"
                component={renderInput}
                label="Admision Number"

              />

              <Field
                name="class"
                type="text"
                component={renderInput}
                label="Class"

              />
              <Field
                name="batch"
                type="text"
                component={renderInput}
                label="Batch"

              />



              <Field
                name="admisionDate"
                component={renderDatePicker}
                label="Admision Date"
                type="date"
                keyboard="default"
                change={onDateChange}
              />
              <Field
                name="guardianName"
                component={renderInput}
                label="Guardian Name"
                type="text"


              />

            </Body>
          </CardItem>

          <View>
              <TouchableHighlight onPress={handleSubmit(onSubmit)} style={styles.submitButton}>
                <Text style={styles.btnText}>Save</Text>
              </TouchableHighlight>

          </View>

        </Card>

      </Content>

    </Container>


  );
}


const styles = StyleSheet.create({
  formItem: {
    width: 300,
    marginVertical: 10,

  },

  submitButton: {
    flex: 1,
    backgroundColor: "#22dd7f",
    justifyContent: 'center',
    alignItems: 'center',
    width: 300,
    height: 25,
    marginTop:10,
    marginLeft:35,
    marginBottom:10,
  },
  btnText: {
    color: 'white',
    fontSize: 19,

  },
  textDanger: {
    color: 'red'
  },
  btnPosition: {
    alignSelf: 'center',
    flex: 1
  },
  field: {
    flex: 1,
    flexDirection: 'row',
    width: 200,
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center'
  },
  placeholder: {
    flex: 1,
    flexDirection: 'column',
    height: 50,
    justifyContent: 'center',


  },
  inputs: {


  },
  formHeader:{
    color:'#141823',
    fontSize:21,

  },
  
  fieldLablel:{
    paddingLeft:5
  }

});


GeneralDetailsForm = reduxForm({
  form: 'generalDetailsForm', // a unique identifier for this form
  validate,
  enableReinitialize: true

})(GeneralDetailsForm)


GeneralDetailsForm = connect(
  state => ({
    initialValues: state.generalDetail.generalDetailsForm,
  }),
)(GeneralDetailsForm)






export default GeneralDetailsForm