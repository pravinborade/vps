import { createStackNavigator, createSwitchNavigator,createAppContainer,createDrawerNavigator } from "react-navigation";
import { faBell, faBook, faUsersClass, faSchool, faUsers, faCalendarAlt,faUserTie, faPollH, faBookReader, faCog, faCoins, faUserGraduate, faBars, faSignOutAlt, faChartLine, faRupeeSign, } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { DrawerItems,  } from 'react-navigation'
import React from 'react';
import { Container, Header, Left, Body, Right, Button, Icon, Title,Content,ListItem, } from 'native-base';
import {View,Text,StyleSheet} from 'react-native'
import AuthLoadingPage from '../Pages/auth/AuthLoadingPage';
import AuthHomePage from "../Pages/auth/AuthHomePage";
import LogInPage from '../Pages/auth/LogInPage';
import ProfileImage from '../Components/app/ProfileImage';
import AsyncStorage from "@react-native-community/async-storage";
import HomePage from "../Pages/app/HomePage";
import ForgetPasswordPage from "../Pages/app/ForgetPasswordPage";
import StudentPersonalInfoPage from '../Pages/app/StudentPersonalInfoPage';
import StudentParentPage from '../Pages/app/StudentParentPage';
import PasswordPage from '../Pages/auth/PasswordPage';
import CreateNewAccountPage from '../Pages/app/AddAminAccountPage';
import TeacherListPage from '../Pages/app/StaffListPage';
import StudentListPage from '../Pages/app/StudentListPage';
import AccountPage from '../Pages/app/AccountPage';
import StudentDetail from '../Components/app/Student/StudentDetail';
import UserListPage from '../Pages/app/UserListPage';
import AddAminAccountPage from '../Pages/app/AddAminAccountPage';
import StaffListPage from '../Pages/app/StaffListPage';
import StaffDetailPage from '../Pages/app/StaffDetailPage';
import StudentProfilePage from '../Components/app/Student/StudentProfilePage';


const  logout = (navigations) =>  {
  
  AsyncStorage.removeItem('userToken')
  navigations.navigate('AuthLoading')
}
const admin=(navigations)=>{
  navigations.navigate('CreateNewAccount')
}

const DrawerContent = (props) => (
  


  <View>
    
      <View style={{
            backgroundColor: 'white',
            height:140,
            alignItems: 'center',
            justifyContent: 'center',
          }} >
          <View>
            <ProfileImage navigations={props.navigation} />
          </View>
      </View>
    
      <DrawerItems {...props} />
   
    {/* <Container >
    <Content> */}
      
        
      <ListItem icon style={{width:'auto'}} button  onPress={ () => props.navigation.navigate('UserList')}>
        <Left>
          <Button style={{ backgroundColor: "#007AFF" }} >
            <Icon active name="person"  />
          </Button>
        </Left>
        <Body>
          <Text>Administrator</Text>
        </Body>
        {/* <Right>
          <Switch value={false} />
        </Right> */}
      </ListItem>
      <ListItem icon style={{width:'auto'}} button  onPress={ () => props.navigation.navigate('UserList')}>
        <Left>
          <Button style={{ backgroundColor: "#007AFF" }} >
          <FontAwesomeIcon icon={ faCalendarAlt} style={styles.IconColor} size={25}/>
          </Button>
        </Left>
        <Body>
          <Text>Calender</Text>
        </Body>
        {/* <Right>
          <Switch value={false} />
        </Right> */}
      </ListItem>
      
      <ListItem icon button  onPress={ () => props.navigation.navigate('StaffList')} >
        <Left>
          <Button style={{ backgroundColor: "#007AFF" }} >
          <FontAwesomeIcon icon={ faUserTie} style={styles.IconColor} size={25}/>
           
          </Button>
        </Left>
        <Body>
          <Text >Teacher</Text> 
        </Body>
        {/* <Right>
          <Text>GeekyAnts</Text>
          <Icon active name="arrow-forward" />
        </Right> */}
      </ListItem>
      <ListItem icon button  onPress={ () => props.navigation.navigate('StudentList')}>
        <Left>
          <Button style={{ backgroundColor: "#007AFF" }} >
          <FontAwesomeIcon icon={ faUserGraduate} style={styles.IconColor}  size={25}/>
          </Button>
        </Left>
        <Body>
          <Text>Students</Text>
        </Body>
        {/* <Right>
          <Text>On</Text>
          <Icon active name="person" />
        </Right> */}
      </ListItem>
      <ListItem icon button  onPress={ () => props.navigation.navigate('StudentList')}>
        <Left>
          <Button style={{ backgroundColor: "#007AFF" }} >
          <FontAwesomeIcon icon={ faUserTie}  style={styles.IconColor} size={25}/>
          </Button>
        </Left>
        <Body>
          <Text>Human Resource</Text>
        </Body>
        {/* <Right>
          <Text>On</Text>
          <Icon active name="person" />
        </Right> */}
      </ListItem>
      <ListItem icon button  onPress={ () => props.navigation.navigate('StudentList')}>
        <Left>
          <Button style={{ backgroundColor:"#007AFF" }} >
          <FontAwesomeIcon icon={faBell  } style={styles.IconColor} size={25}/>
          </Button>
        </Left>
        <Body>
          <Text>News</Text>
        </Body>
        {/* <Right>
          <Text>On</Text>
          <Icon active name="person" />
        </Right> */}
      </ListItem>
      <ListItem icon button  onPress={ () => props.navigation.navigate('StudentList')}>
        <Left>
          <Button style={{ backgroundColor: "#007AFF" }} >
          <FontAwesomeIcon icon={ faCog} style={styles.IconColor} size={25}/>
          </Button>
        </Left>
        <Body>
          <Text>Settings</Text>
        </Body>
        {/* <Right>
          <Text>On</Text>
          <Icon active name="person" />
        </Right> */}
      </ListItem>
      <ListItem icon button onPress={()=>logout(props.navigation)} >
        <Left>
          <Button style={{ backgroundColor: "#ff6347" }} transparent >
            <Icon active name="log-out" onPress={()=>logout(props.navigation)} />
          </Button>
        </Left>
        <Body>
          <Text>Logout</Text>
        </Body>
        {/* <Right>
          <Text>On</Text>
          <Icon active name="arrow-forward" />
        </Right> */}
      </ListItem>
    {/* </Content>
 
  
</Container> */}
      
  </View>
)


export const DrawerStack = createDrawerNavigator({
  Home:HomePage,
},
{
  initialRouteName:'Home',
  hideStatusBar: false,
  overlayColor: '#009999',
  contentOptions: {
   activeTintColor: '#fff',
    activeBackgroundColor: '#009999'
  },
  contentComponent:DrawerContent
});

const AppStack = createStackNavigator({ 
 
   Home:DrawerStack,
   StudentPersonalInfo:StudentPersonalInfoPage,
   StudentParent:StudentParentPage,
   CreateNewAccount:CreateNewAccountPage,
   TeacherList:TeacherListPage,
   StudentList:StudentListPage,
   Account:AccountPage,
   Student:StudentDetail,
   UserList:UserListPage,
   AddAminAccount:AddAminAccountPage,
   StaffList:StaffListPage,
   StaffDetail:StaffDetailPage,
   StudentProfile:StudentProfilePage,
  
   
},{
  initialRouteName:'Home',
  headerMode:'none',
  navigationOptions:{
    headerVisible:false
  }

});
const AuthStack = createStackNavigator({ 
  LogIn: LogInPage,
  AuthHome:AuthHomePage,
  Reset:ForgetPasswordPage,
  Password:PasswordPage,
},{
  initialRouteName:'AuthHome'
});
export const  AppContainer = createAppContainer(createSwitchNavigator(
  {
    AuthLoading: AuthLoadingPage ,
    App: AppStack,
    Auth: AuthStack,
  },
  {
    initialRouteName: 'AuthLoading',
  }
)); 

const styles = StyleSheet.create({
  Title:{
    color:'#000',
    fontSize: 20,
    fontFamily:'italic',
    paddingHorizontal:20,
    paddingVertical:30,
 
  },
  IconColor:{
    color:'#FFF',
    
  }
})
