export function addUser(user) {
    return {
        type: "ADD_USER",
        user
    }
}
export function updateUser(user) {

    return {

        type: "UPDATE_USER",
        user

    }


}

export function updatePassword(user) {

    return {
        type: "UPDATE_PASSWORD",
        user
    }
}

export function saveUser(user) {

    return {
        type: "SAVE_USER",
        user
    }
}

import { AsyncStorage } from 'react-native';

export const getToken = (token) => ({

    type: 'GET_TOKEN',
    token,
});

export const saveToken = token => ({
    type: 'SAVE_TOKEN',
    token
});

export const removeToken = () => ({
    type: 'REMOVE_TOKEN',
});

export const loading = bool => ({
    type: 'LOADING',
    isLoading: bool,
});

export const error = error => ({
    type: 'ERROR',
    error,
});



export const getUserToken = () => dispatch => (

    AsyncStorage.getItem('userToken')
        .then((data) => {
            dispatch(loading(false));
            dispatch(getToken(data));
        })
        .catch((err) => {
            dispatch(loading(false));
            dispatch(error(err.message || 'ERROR'));
        })
)


export const saveUserToken = (data) => dispatch => {

    return AsyncStorage.setItem('userToken', data)
        .then((data) => {

            dispatch(loading(false));
            dispatch(saveToken('token saved'));
        })
        .catch((err) => {

            dispatch(loading(false));
            dispatch(error(err.message || 'ERROR'));
        })
}



export const removeUserToken = () => dispatch =>
    AsyncStorage.removeItem('userToken')
        .then((data) => {
            dispatch(loading(false));
            dispatch(removeToken());
        })
        .catch((err) => {
            dispatch(loading(false));
            dispatch(error(err.message || 'ERROR'));
        })


