export function addParent(parent){

    return{
        type:"ADD_PARENT",
        parent
    }

}

export function updateParent(parent){
    return{
        type:"UPDATE_PARENT",
        parent
    }

}