export function addStudentList(students){

    return{
        type:"ADD_STUDENT_LIST",
        students
    }

}

export function addStudent(student){
    
    return{
        type:"ADD_STUDENT",
        student
    }

}

export function saveStudent(student){
    return{
        type:"SAVE_STUDENT",
        student
    }

}