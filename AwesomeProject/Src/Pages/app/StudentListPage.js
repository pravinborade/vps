import React, { Component } from 'react';
import { Container, Header, Content, List, ListItem, Left, Body, Right, Thumbnail, Text, Button, Title } from 'native-base';
import { StyleSheet, View } from 'react-native';
import { toHtml } from '@fortawesome/fontawesome-svg-core';
import { connect } from 'react-redux';
import { addStudentList } from './../../Actions/studentActions';
import axios from 'axios';
import { baseURL, jsonHeader } from '../../Config';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faArrowLeft, faPlus } from '@fortawesome/free-solid-svg-icons';
import moment from 'moment';




class StudentListPage extends Component {

  constructor(props) {
    super(props);
    this.state = {

    }

  }




  componentDidMount() {
  
    axios.get(`${baseURL}/api/student/all`, jsonHeader)
    .then( (result) => {
     
        
        this.props.addStudentList(result.data);
    }).catch((error) => {
    
      this.setState({
        isLoaded: true,
        error
      });
    })
  }

   addStudent(id){
  
    this.props.navigation.navigate('Student', {
      Id: id,
  });
   
  }

  render() {

    return (
      <Container>
        <Header>
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()} >
              <FontAwesomeIcon icon={faArrowLeft} style={styles.icon} size={20} />
            </Button>
          </Left>
          <Body>
            <Title>studentList</Title>
          </Body>
          <Right>
            <FontAwesomeIcon icon={faPlus} size={20} style={styles.IconColor} onPress={() => this.addStudent(0)} />
          </Right>
        </Header>
        <Content>
          <View>
            <List>
              {
                this.props.studentList.map((stud, i) => {
                  return (
                     <ListItem avatar key={i} onPress={() => this.addStudent(stud.id)}>
                        
                      <Left/>
                      <Body >
                        <Text  style={styles.titleText}>
                          {stud.firstName} {stud.lastName}
                        </Text>

                      </Body>
                      <Right />
                    </ListItem>
                  );
                })
              }

            </List>
          </View>
        </Content>
      </Container>
    );
  }
}



const styles = StyleSheet.create({

  icon: {
    color: '#FFF',
    paddingBottom: 0,
    paddingLeft: 10,


  },
  titleText: {

    fontFamily: 'Cochin',
    paddingBottom: 0,


  },
  IconColor: {
    color: '#FFF',
  },
})


const mapStateToProps = (state) => {

  return {

    studentList: state.student.studentList

  }
}


const mapDispatchToProps = (dispatch) => {

  return {

    addStudentList: (student) => { dispatch(addStudentList(student)) },

  }

}


export default connect(mapStateToProps, mapDispatchToProps)(StudentListPage)
