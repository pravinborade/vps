import React, { Component } from 'react';
import { DrawerNavigator } from "react-navigation";
import { createStackNavigator, createSwitchNavigator, createAppContainer, createDrawerNavigator } from "react-navigation";
import {
  StyleSheet,
  View,
  TextInput,
  TouchableHighlight,
  Alert,
  Dimensions,

  Image, ImageBackground, Text
} from 'react-native';
import { StatusBar } from "react-native";
import {
  Container, Header,
  Title, Left, Icon, Right,
  Button, Body, Content,
  Footer,
  FooterTab, Accordion, CardItem, Thumbnail, Card, ListItem, Switch,
  Separator,Tab,Tabs,TabHeading
} from "native-base";
import { } from 'native-base';

import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { connect } from 'react-redux';
import { AsyncStorage } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import StudentProfileForm from '../../Components/app/forms/StudentProfileForm'

import { faBars, faUserGraduate, faUserTie, faClock, faRupeeSign, faImages, faClipboardCheck, faVolumeUp, faBookmark, faClipboardList, faBlog, faLandmark, faPoll, faComment, faBirthdayCake, faSms, faBell, faHandshake, faPollH, faHandHoldingUsd, faCalendar, faCalendarDay, faAngleRight } from '@fortawesome/free-solid-svg-icons';
import { removeUserToken } from "../../Actions/authActions";
import { updatePassword } from './../../Actions/authActions';
import {ContactPage} from  '../../Pages/app/ContactPage';



class HomePage extends Component {

  constructor(props) {
    super(props);
    this.state = {


    }
  }


  submit = values => {
    console.log(values);

  }


  drawerOpen() {


    this.props.navigation.openDrawer();
  }
  drawerClose() {

    this.props.navigation.closeDrawer();
  }

  logout = () => {

    this.props.removeUserToken();

    this.props.navigation.navigate('AuthLoading');

  }
  render() {
    
    return (
      <Container>
        <Header >
          <Left>
            <Button onPress={() => this.drawerOpen()} >
              <FontAwesomeIcon icon={faBars} size={20}
                style={styles.IconColor} onPress={() => this.drawerOpen()}
              />
            </Button>
          </Left>
          <Body>
            <Title>Home</Title>
          </Body>
          <Right />
        </Header>
        <Content>

        <ListItem icon style={styles.image} button  onPress={ () => this.props.navigation.navigate('StudentProfile')}  >
            <Left>
              
            <View>
              <Image style={styles. userProfile}  
              source={{uri: 'https://bootdey.com/img/Content/avatar/avatar6.png'}}/>
            </View>
              
            </Left>
            <Body style={{borderColor:'white'}}>
              <Text> Pravin Borade</Text>
              <Text></Text>
            </Body>
            <Right  style={styles.iconPosition} >
              <FontAwesomeIcon icon={faAngleRight} size={40} />
            </Right>
          </ListItem>
          <Separator bordered style={styles.seperator}>
          </Separator>

          <ListItem icon >
            <Left>
              <Button style={{ backgroundColor: "#007AFF" }}>
                <FontAwesomeIcon icon={faClock} size={20} style={styles.IconColor} />
              </Button>
            </Left>
            <Body>
              <Text>TimeTable</Text>
            </Body>
            <Right>
              <FontAwesomeIcon icon={faAngleRight} size={20} />
            </Right>
          </ListItem>
          <Separator bordered style={styles.seperator}>
          </Separator>
          <ListItem icon>
            <Left>
              <Button style={{ backgroundColor: "#007AFF" }}>
                <FontAwesomeIcon icon={faCalendarDay} size={20} style={styles.IconColor} />

              </Button>
            </Left>
            <Body>
              <Text>Events</Text>
            </Body>
            <Right>
              <FontAwesomeIcon icon={faAngleRight} size={20} />
            </Right>
          </ListItem>
          <Separator bordered style={styles.seperator}>
          </Separator>
          <ListItem icon>
            <Left>
              <Button style={{ backgroundColor: "#007AFF" }}>
                <FontAwesomeIcon icon={faClipboardCheck} size={20} style={styles.IconColor} />
              </Button>
            </Left>
            <Body>
              <Text>Attendence</Text>
            </Body>
            <Right>

              <FontAwesomeIcon icon={faAngleRight} size={20} />
            </Right>
          </ListItem>
          <Separator bordered style={styles.seperator}>
          </Separator>
          <ListItem icon>
            <Left>
              <Button style={{ backgroundColor: "#007AFF" }}>
                <FontAwesomeIcon icon={faRupeeSign} size={20} style={styles.IconColor} />
              </Button>
            </Left>
            <Body>
              <Text>Fees</Text>
            </Body>
            <Right>

              <FontAwesomeIcon icon={faAngleRight} size={20} />
            </Right>
          </ListItem>
          <Separator bordered style={styles.seperator}>
          </Separator>
          <ListItem icon>
            <Left>
              <Button style={{ backgroundColor: "#007AFF" }}>
                <FontAwesomeIcon icon={faVolumeUp} size={20} style={styles.IconColor} />
              </Button>
            </Left>
            <Body>
              <Text>Announcements</Text>
            </Body>
            <Right>

              <FontAwesomeIcon icon={faAngleRight} size={20} />
            </Right>
          </ListItem>
          <Separator bordered style={styles.seperator}>
          </Separator>
          <ListItem icon>
            <Left>
              <Button style={{ backgroundColor: "#007AFF" }}>
                <FontAwesomeIcon icon={faBookmark} size={20} style={styles.IconColor} />
              </Button>
            </Left>
            <Body>
              <Text>My Subject</Text>
            </Body>
            <Right>

              <FontAwesomeIcon icon={faAngleRight} size={20} />
            </Right>
          </ListItem>
          <Separator bordered style={styles.seperator}>
          </Separator>
          <ListItem icon>
            <Left>
              <Button style={{ backgroundColor: "#007AFF" }}>
                <FontAwesomeIcon icon={faImages} size={20} style={styles.IconColor} />
              </Button>
            </Left>
            <Body>
              <Text>Gallery</Text>
            </Body>
            <Right>

              <FontAwesomeIcon icon={faAngleRight} size={20} />
            </Right>
          </ListItem>
          <Separator bordered style={styles.seperator}>
          </Separator>
          <ListItem icon>
            <Left>
              <Button style={{ backgroundColor: "#007AFF" }}>
                <FontAwesomeIcon icon={faClipboardList} size={20} style={styles.IconColor} />
              </Button>
            </Left>
            <Body>
              <Text>Exam Report</Text>
            </Body>
            <Right>

              <FontAwesomeIcon icon={faAngleRight} size={20} />
            </Right>
          </ListItem>
          {/* <Separator bordered style={styles.seperator}>
          </Separator> */}



          {/* <ListItem icon >
            <Left>
              <Button style={{ backgroundColor: "#007AFF" }}>
                <FontAwesomeIcon icon={faUserTie} size={20} style={styles.IconColor} />
              </Button>
            </Left>
            <Body>
              <Text>Admitted Employees</Text>
            </Body>
            <Right>
            <FontAwesomeIcon icon={faAngleRight} size={20}  />
            </Right>
          </ListItem>
          <Separator bordered style={styles.seperator}>
          </Separator>
          <ListItem icon>
            <Left>
              <Button style={{ backgroundColor: "#007AFF" }}>
                <FontAwesomeIcon icon={faBirthdayCake} size={20} style={styles.IconColor} />
              </Button>
            </Left>
            <Body>
              <Text>Birthdays</Text>
            </Body>
            <Right>

            <FontAwesomeIcon icon={faAngleRight} size={20}  />
            </Right>
          </ListItem>
          <Separator bordered style={styles.seperator}>
          </Separator>
          <ListItem icon>
            <Left>
              <Button style={{ backgroundColor: "#007AFF" }}>
                <FontAwesomeIcon icon={faRupeeSign} size={20} style={styles.IconColor} />
              </Button>
            </Left>
            <Body>
              <Text>Book Return Due</Text>
            </Body>
            <Right>

            <FontAwesomeIcon icon={faAngleRight} size={20}  />
            </Right>
          </ListItem>
          <Separator bordered style={styles.seperator}>
          </Separator>
          <ListItem icon>
            <Left>
              <Button style={{ backgroundColor: "#007AFF" }}>
                <FontAwesomeIcon icon={faBlog} size={20} style={styles.IconColor} />
              </Button>
            </Left>
            <Body>
              <Text>Blogs</Text>
            </Body>
            <Right>

            <FontAwesomeIcon icon={faAngleRight} size={20}  />
            </Right>
          </ListItem>
          <Separator bordered style={styles.seperator}>
          </Separator>
          <ListItem icon>
            <Left>
              <Button style={{ backgroundColor: "#007AFF" }}>
                <FontAwesomeIcon icon={faClipboardList} size={20} style={styles.IconColor} />
              </Button>
            </Left>
            <Body>
              <Text>Examinations</Text>
            </Body>
            <Right>

            <FontAwesomeIcon icon={faAngleRight} size={20}  />
            </Right>
          </ListItem>
          <Separator bordered style={styles.seperator}>
          </Separator>
          <ListItem icon>
            <Left>
              <Button style={{ backgroundColor: "#007AFF" }}>
                <FontAwesomeIcon icon={faUserGraduate} size={20} style={styles.IconColor} />
              </Button>
            </Left>
            <Body>
              <Text>Absent Students</Text>
            </Body>
            <Right>

            <FontAwesomeIcon icon={faAngleRight} size={20}  />
            </Right>
          </ListItem>
          <Separator bordered style={styles.seperator}>
          </Separator> */}


          {/* <ListItem icon>
            <Left>
              <Button style={{ backgroundColor: "#007AFF" }}>
                <FontAwesomeIcon icon={faRupeeSign} size={20} style={styles.IconColor} />
              </Button>
            </Left>
            <Body>
              <Text>Fees Due</Text>
            </Body>
            <Right>

            <FontAwesomeIcon icon={faAngleRight} size={20}  />
            </Right>
          </ListItem>
          <Separator bordered style={styles.seperator}>
          </Separator>
          
          <Separator bordered style={styles.seperator}>
          </Separator> */}





          {/* <ListItem icon>
            <Left>
              <Button style={{ backgroundColor: "#007AFF" }}>
                <FontAwesomeIcon icon={faLandmark} size={20} style={styles.IconColor} />

              </Button>
            </Left>
            <Body>
              <Text>Finance</Text>
            </Body>
            <Right>

            <FontAwesomeIcon icon={faAngleRight} size={20}  />
            </Right>
          </ListItem>
          <Separator bordered style={styles.seperator}>
          </Separator> */}
          {/* <ListItem icon>
            <Left>
              <Button style={{ backgroundColor: "#007AFF" }}>
                <FontAwesomeIcon icon={faBell} size={20} style={styles.IconColor} />
              </Button>
            </Left>
            <Body>
              <Text>News</Text>
            </Body>
            <Right>

            <FontAwesomeIcon icon={faAngleRight} size={20}  />
            </Right>
          </ListItem>
          <Separator bordered style={styles.seperator}>
          </Separator>
          <ListItem icon>
            <Left>
              <Button style={{ backgroundColor: "#007AFF" }}>
                <FontAwesomeIcon icon={faHandshake} size={20} style={styles.IconColor} />
              </Button>
            </Left>
            <Body>
              <Text>Online Meetings</Text>
            </Body>
            <Right>

               <FontAwesomeIcon icon={faAngleRight} size={20}  />
            </Right>
          </ListItem>
          <Separator bordered style={styles.seperator}>
          </Separator>
          <ListItem icon>
            <Left>
              <Button style={{ backgroundColor: "#007AFF" }}>
                <FontAwesomeIcon icon={faClipboardCheck} size={20} style={styles.IconColor} />
              </Button>
            </Left>
            <Body>
              <Text>Leave Applications</Text>
            </Body>
            <Right>

            <FontAwesomeIcon icon={faAngleRight} size={20}  />
            </Right>
          </ListItem>
          <Separator bordered style={styles.seperator}>
          </Separator> */}
          {/* <ListItem icon>
            <Left>
              <Button style={{ backgroundColor: "#007AFF" }}>
                <FontAwesomeIcon icon={faPoll} size={20} style={styles.IconColor} />

              </Button>
            </Left>
            <Body>
              <Text>Polls</Text>
            </Body>
            <Right>

            <FontAwesomeIcon icon={faAngleRight} size={20}  />
            </Right>
          </ListItem>
          <Separator bordered style={styles.seperator}>
          </Separator> */}
          {/* <ListItem icon>
            <Left>
              <Button style={{ backgroundColor: "#007AFF" }}>
                <FontAwesomeIcon icon={faUserGraduate} size={20} style={styles.IconColor} />
              </Button>
            </Left>
            <Body>
              <Text>Relived Students</Text>
            </Body>
            <Right>

             <FontAwesomeIcon icon={faAngleRight} size={20}  />
            </Right>
          </ListItem>
          <Separator bordered style={styles.seperator}>
          </Separator> */}
          {/* <ListItem icon>
            <Left>
              <Button style={{ backgroundColor: "#007AFF" }}>
                <FontAwesomeIcon icon={faImages} size={20} style={styles.IconColor} />
              </Button>
            </Left>
            <Body>
              <Text>Photos Added</Text>
            </Body>
            <Right>

            <FontAwesomeIcon icon={faAngleRight} size={20}  />
            </Right>
          </ListItem>
          <Separator bordered style={styles.seperator}>
          </Separator> */}
          {/* <ListItem icon>
            <Left>
              <Button style={{ backgroundColor: "#007AFF" }}>
                <FontAwesomeIcon icon={faSms} size={20} style={styles.IconColor} />
              </Button>
            </Left>
            <Body>
              <Text>SMS Sent</Text>
            </Body>
            <Right>

            <FontAwesomeIcon icon={faAngleRight} size={20}  />
            </Right>
          </ListItem>
          <Separator bordered style={styles.seperator}>
          </Separator> */}
          {/* <ListItem icon>
            <Left>
              <Button style={{ backgroundColor: "#007AFF" }}>
                <FontAwesomeIcon icon={faHandHoldingUsd} size={20} style={styles.IconColor} />
              </Button>
            </Left>
            <Body>
              <Text>Tasks Due</Text>
            </Body>
            <Right>

            <FontAwesomeIcon icon={faAngleRight} size={20}  />
            </Right>
          </ListItem> 
          <Separator bordered style={styles.seperator}>
          </Separator>*/}

          {/* <Separator bordered style={styles.seperator}>
          </Separator>
          
          
          <Separator bordered style={styles.seperator}>
          </Separator> */}
          {/* <ListItem icon>
            <Left>
              <Button style={{ backgroundColor: "#007AFF" }}>
                <FontAwesomeIcon icon={faPollH} size={20} style={styles.IconColor} />
              </Button>
            </Left>
            <Body>
              <Text>Placements</Text>
            </Body>
            <Right>

            <FontAwesomeIcon icon={faAngleRight} size={20}  />
            </Right>
          </ListItem> */}
          {/* <Separator bordered style={styles.seperator}>
          </Separator>
         
          <Separator bordered style={styles.seperator}>
          </Separator>
         
          <ListItem icon>
            <Left>
              <Button style={{ backgroundColor: "#007AFF" }}>
                <FontAwesomeIcon icon={faComment} size={20} style={styles.IconColor} />
              </Button>
            </Left>
            <Body>
              <Text>Discussions</Text>
            </Body>
            <Right>

            <FontAwesomeIcon icon={faAngleRight} size={20}  />
            </Right>
          </ListItem>
          <Separator bordered style={styles.seperator}>
          </Separator>
          
          <ListItem icon>
            <Left>
              <Button style={{ backgroundColor: "#007AFF" }}>
                <FontAwesomeIcon icon={faUserTie} size={20} style={styles.IconColor} />
              </Button>
            </Left>
            <Body>
              <Text>Relived Employees</Text>
            </Body>
            <Right>

            <FontAwesomeIcon icon={faAngleRight} size={20}  />
            </Right>
          </ListItem>
          <Separator bordered style={styles.seperator}>
          </Separator> */}
        </Content>
        <Footer >
          <FooterTab style={{backgroundColor:"#FFF"}}>
           <Tabs >
             <Tab style={{backgroundColor:"#FFF"}} heading={ <TabHeading><Icon name="camera" /></TabHeading>}>
             <FontAwesomeIcon icon={faAngleRight} size={40} />

             </Tab>
           </Tabs>
         
          </FooterTab>
        </Footer>
      </Container>


    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: 'orange',

  },


  backgroundTransperant: {
    backgroundColor: 'orange'
  },


  IconColor: {
    color: 'white'
  },
  item: {
    marginTop: 10,
    marginBottom: 10
  },
  seperator: {
    height: 5,
    paddingTop: 5,
    paddingBottom: 5
  },     
  image:{
  backgroundColor: 'white',
  height:100,
  
  },
  iconPosition:{
    paddingVertical:50,
    borderColor:'white'


  },
  userProfile: {
    width: 70,
    height: 70,
    borderRadius: 63,
  //   borderWidth: 4,
    borderColor: "white",
    alignSelf:'center',
    

  },

})

const mapStateToProps = (state) => {
  return {}
}
const mapDispatchToProps = (dispatch) => {
  return {

    removeUserToken: () => { dispatch(removeUserToken()) },

  }
}
export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
