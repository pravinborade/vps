import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableHighlight,
  Image,
  Alert
} from 'react-native';
import {
    Container, Header,
    Title, Left, Icon, Right,
    Button, Body, Content,
    Footer,
    FooterTab 
  } from "native-base";

import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
import {baseURL,jsonHeader} from '../../Config';
import AppHeader from '../../Components/AppHeader';


class CreateNewAccountPage extends Component {
    constructor(props) {
      super(props);
      this.state = {
        phone_number  : '',
       
        formError:[],
      }
    }
 

     
      
    render() {
       return   (
         <Container >
          {/* <AppHeader  title={"Create New Account"} navigation={this.props.navigation} />  */}
          <View style={styles.container} >
          <Image
            style={{width: 100, height: 100}}
            source={require('../../Assets/vps-new-logo.png')}
          />
      </View> 
        <Content >
            <View  >
             <Text>You have created account successfully.</Text>
            </View>
        </Content> 
    </Container>
        );
    }
}
 

const styles= StyleSheet.create({

    container:{
       flex:1,
        flexDirection:'column',
        justifyContent:'center',
        alignItems:'center'   
        
    },
    center:{
      justifyContent:'center',
      alignItems:'center'   
      
    },
    
    title:{
      fontSize:20,
      color:"#000",
      paddingTop:0
    },
    logoName:{
     height:100,
     width:100,
     marginTop:10 
    },
    IconColor:{
        color:'white'
    }
})

const mapStateToProps=(state) =>{
  return {  }
}


const mapDispatchToProps=(dispatch) =>{
  return {

   
  }
}
export default connect(mapStateToProps, mapDispatchToProps) (CreateNewAccountPage)