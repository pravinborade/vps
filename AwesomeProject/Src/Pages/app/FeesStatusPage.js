import React, { Component } from 'react';
import {StyleSheet,TextInput} from 'react-native';
import { Container, Header, 
  Title, Left, Icon, Right, 
  Button, Body, Content, 
  Footer,Item,Input,
  FooterTab,Text, View,List,ListItem,Thumbnail,Card,CardItem
} from "native-base";
import { connect } from 'react-redux';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import {faArrowLeft} from '@fortawesome/free-solid-svg-icons';
import {addFees,updateFees} from '../../../Src/Actions/feesAction';
import { TouchableHighlight } from 'react-native-gesture-handler';
class FeesStatusPage extends Component {

    constructor(props) {    
    super(props);
    this.state = {
    clicked:0,
    search:'false',
    avatarSource:null ,
    }
  }
    
 render() {
return (
<Container>
  <Header>
    <Left>
    <Button transparent onPress={ () => this.props.navigation.goBack()}>
      <FontAwesomeIcon icon={faArrowLeft} style={styles.icon} size={20}/> 
    </Button>
    </Left>
    <Body>
    <Title>
        Fees Status
    </Title>
    </Body>
<Right/>     
</Header>
<Content>
  <View style={{ flex: 1,flexDirection: 'row',justifyContent:'flex-end'}}>
  <Button small success>
    <Text style={styles.textTop} >Print</Text>
  </Button>
  </View>
  <View>
  <Card>
    <CardItem>
      <Body>
        <Text>
          Total Fees:{this.props.fees.totalFees}
        </Text>
      </Body>
    </CardItem>       
    </Card>
    <Card>
      <CardItem>
        <Body>
          <Text>
          Fees Paid:{this.props.fees.paidFees}
          </Text>
        </Body>
      </CardItem>
    </Card>
    <Card>
    <CardItem>
      <Body>
        <Text>
          Pending Fees:{this.props.fees.pendingFees}
        </Text>
      </Body>
    </CardItem>
    </Card>
    </View>
    <View style={{ flex: 1,flexDirection: 'row',justifyContent:'flex-end'}}>
      <Text style={styles.textBottom}>Signature</Text>

      </View>
    </Content>    
    </Container>

    );
    }
}
const styles= StyleSheet.create({

      container:{
        flex:1, 
        justifyContent:"center",
        alignItems:"center",

      },
      userInfo:{
        flex:1,
        justifyContent:'center',
        textAlign:'center',
        width:150,
        height:40,
        color:'green',
        borderBottomColor:'red',
        alignSelf:'center'
      },
      icon:{
        color:'white',
      },
      textColor:{
          color:'white',
      },
      textBottom:{
        paddingTop:60,
        paddingRight:10,

      },
      textTop:{
        paddingTop:0,
        paddingRight:10,
        color:'#7fff00',
        fontSize:20,
        textAlign:'center',
        color:'#FFF'
      }
      
})
const mapStateToProps=(state) =>{

  return {
      fees:state.fees.studFees
    //   stud:state.student.studentList      
  }
}
const mapDispatchToProps=(dispatch) =>{
 
  return {
       
    }
}

export default connect(mapStateToProps, mapDispatchToProps) (FeesStatusPage)

