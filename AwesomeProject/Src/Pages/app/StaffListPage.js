import React, { Component } from 'react';
import { Container, Header, Content, List, ListItem, Left, Body, Right, Thumbnail, Text, Button, Title, Icon } from 'native-base';
import { StyleSheet, View } from 'react-native';
import { faArrowLeft,faPlus } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { connect } from 'react-redux';
import axios from 'axios';
import { baseURL, jsonHeader } from '../../Config';
import {addStaffList} from '../../Actions/staffActions';



class StaffListPage extends Component {

  constructor(props) {
    super(props);
    this.state = {

    }

  }



  componentDidMount() {
     debugger;
    axios.get(`${baseURL}/api/teacher/all`, jsonHeader)
    .then( (result) => {
     

        this.props.addStaffList(result.data);
    }).catch((error) => {
       debugger
      this.setState({
        isLoaded: true,
        error
      });
    })
  }

  addStaff(id){
  
    this.props.navigation.navigate('StaffDetail', {
      Id: id,
  });
   
  }
  render() {

    return (
      <Container>
        <Header>
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()} >
              <FontAwesomeIcon icon={faArrowLeft} style={styles.icon} size={20} />
            </Button>
          </Left>
          <Body>
            <Title>Staff List</Title>
          </Body>
          <Right>
            <FontAwesomeIcon icon={faPlus} size={20} style={styles.iconColor} onPress={() => this.addStaff(0)} />
          </Right>
        </Header>
        <Content>
        <View>
            <List>
              {
                this.props.staffList.map((staff, i) => {
                  return (
                     <ListItem avatar key={i} onPress={() => this.addStaff(staff.id)}>
                      
                      <Left/>
                      <Body >
                        <Text  style={styles.titleText}>
                          {staff.firstName} {staff.lastName}
                        </Text>

                      </Body>
                      <Right />
                    </ListItem>
                  );
                })
              }

            </List>
          </View>
        </Content>
      </Container>
    );
  }
}



const styles = StyleSheet.create({

  icon: {
    color: '#FFF',
    paddingBottom: 90,
    paddingLeft: 10,
  },
  titleText: {

    fontFamily: 'Cochin',
    fontWeight: 'bold'
  },
  iconColor:{
    color:'#FFF',
  }
})


const mapStateToProps = (state) => {

  return {

    staffList:state.staff.staffList

  }
}


const mapDispatchToProps = (dispatch) => {

  return {

    addStaffList: (staff) => { dispatch(addStaffList(staff)) }

  }

}


export default connect(mapStateToProps, mapDispatchToProps)(StaffListPage)
