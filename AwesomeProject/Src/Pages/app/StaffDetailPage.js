import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableHighlight,
  Image,
  Alert
} from 'react-native';
import {
  Container, Header,
  Title, Left, Icon, Right,
  Button, Body, Content,
  Footer,
  FooterTab
} from "native-base";

import { connect } from 'react-redux';
import axios from 'axios';
import { baseURL, jsonHeader } from '../../Config';
import AppHeader from '../../Components/AppHeader';
import {addStaff} from '../../Actions/staffActions';
import StaffForm from '../../Components/app/forms/StaffForm';


class StaffDetailPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
     
      formEdit:false,
      formError: [],
    }
    this.getStaff();
  }

     getStaff(){

      var id=this.props.navigation.state.params.Id
   
      if(id>0){
        axios.get(`${baseURL}/api/teacher/`+id, jsonHeader)
     
        .then(
          (result) => {
           
            this.props.addStaff(result.data);
          }).catch(
      
          (error) => {
           
            this.setState({
              isLoaded: true,
              error
            });
          }
        )
      }else{
              this.setState({formEdit:true}) 
          }

     }
 




  submit = values => {
    values.birthDate=values.birthDate.toString();
    const formData = new FormData();

    formData.append('id',values.id)
    formData.append('firstName',values.firstName)
    formData.append('middleName',values.middleName)
    formData.append('lastName',values.lastName)
    formData.append('birthDate',values.birthDate)
    formData.append('mobileNumber',values.mobileNumber)


    return axios.post(`${baseURL}/api/teacher/add`, formData, jsonHeader)
      .then((response) => {
       debugger;
        this.addStaff(response.data)

      })
      .catch((error) => {
         debugger;
       

        }
      )
      .finally(function () {

      });
    
  }
  render() {


    return (
      <Container>
        <AppHeader title={"Staff Detail"} navigation={this.props.navigation} />

        <Content >
          <View>
            <StaffForm onSubmit={this.submit} formErrors={this.state.formError} />
          </View>
        </Content>
      </Container>
    );
  }
}


const styles = StyleSheet.create({



  title: {
    fontSize: 20,
    color: "#000",
    paddingTop: 0
  },
  form: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-around',
    justifyContent: 'center',
    alignItems: 'center'

  }


})

const mapStateToProps = (state) => {
  debugger;
  return {
    
       staff:state.staff.staff     
  }
}



const mapDispatchToProps = (dispatch) => {
  debugger;
  return {

    addStaff:(data) => dispatch(addStaff(data)),
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(StaffDetailPage)