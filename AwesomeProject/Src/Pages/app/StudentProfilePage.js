import React, { Component } from 'react';
import { Container, Header, Content, List, ListItem,  Left, Right, Icon,Button,Body,Title } from 'native-base';

import {
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
   
  } from 'react-native';

import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import {faArrowLeft} from '@fortawesome/free-solid-svg-icons';
import ProfileImageForm from '../../Components/app/forms/ProfileImageForm';



var BUTTONS = [
    { text: "Gallery", icon:"image", iconColor: "#2c8ef4" },
    { text: "Camera", icon:"camera", iconColor: "#f42ced" },
    { text: "Delete", icon: "trash", iconColor: "#fa213b" },
    { text: "Cancel", icon: "close", iconColor: "#25de5b" }
  ];
  var DESTRUCTIVE_INDEX = 1;
  var CANCEL_INDEX = 3;




export default class StudentProfilePage extends Component {

    constructor(props){
    
        super(props);
        this.state={}
        
    }
    
      render() {
        return (
        <Container>
            <View  style={styles.icon}>
                    <Header >
                    
                        <Left >
                            
                            <Button transparent  onPress={ () => this.props.navigation.goBack()}>
                                <FontAwesomeIcon style={styles.icon} icon={faArrowLeft} size={20} /> 
                            </Button>
                        </Left>
                            
                            <Body>
                                <Title>Profile Information</Title>
                            </Body>
                        <Right />
                            
                    </Header>
            </View>
    
               <View>
    
                    <ProfileImageForm  BUTTONS={BUTTONS} DESTRUCTIVE_INDEX={DESTRUCTIVE_INDEX} CANCEL_INDEX={CANCEL_INDEX}  />
    
               </View>
                <Content>
                    <List>
                        <ListItem noIndent style={{ backgroundColor: "#cde1f9" }}>
                                <Left>
                                    <View>
                                        <TouchableOpacity onPress={()=>this.props.navigation.navigate('StudentPersonalInfo')}>
                                        <Text>
                                            Personal Information
                                        </Text>
                                        </TouchableOpacity>
                                    </View>
                                </Left>
                                <Right>
                                    <Icon name="arrow-forward" />
                                </Right>
                        </ListItem>
                        <ListItem >
                            <Left>
                                    <View>
                                    <TouchableOpacity onPress={()=>this.props.navigation.navigate('Address')}>
                                    <Text>
                                        Address
                                    </Text>
                                    </TouchableOpacity>
                                    </View>
                            </Left>
                            <Right>
                                    <Icon name="arrow-forward" />
                            </Right>
                        </ListItem>
                        <ListItem >
                            <Left>
                                    <View>
                                    <TouchableOpacity onPress={()=>this.props.navigation.navigate('Contact')}>
                                    <Text>
                                    Emergency Contact
                                    </Text>
                                    </TouchableOpacity>
                                    </View>
                            </Left>
                            <Right>
                                    <Icon name="arrow-forward" />
                            </Right>
                        </ListItem>
                       
                    </List>
                </Content>
        </Container>
        );
      }
    }
    
    
    const  styles= StyleSheet.create({
    
    icon:{
    
      color:'white',
    
    }
    
    })