import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import { Container, Header, 
  Title, Left, Icon, Right, 
  Button, Body, Content, 
  Footer,
  FooterTab,Text,List,ListItem,Thumbnail
} from "native-base";
import { connect } from 'react-redux';
import { Table, TableWrapper, Row, Rows, Col} from 'react-native-table-component';

import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import {faArrowLeft} from '@fortawesome/free-solid-svg-icons';
import { colors } from 'react-native-elements';
 
 class ProgressReportPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tableHead: [ 'Subject', ' Obtained Mark','Max Mark','Result'],
     
      tableTitle: ['English', 'Marathi', 'Hindi','Math','Science','History','Geography'],
      tableData: [
        [  '80','100','90'],
        [  '95','100','90'],
        [  '90','100','90'],
        [  '90','100','90'],
        [  '90','100','90'],
        [  '90','100','90'],
        [  '90','100','90']
      ]
    }
  }
 
  render() {
    const state = this.state;
    return (

      <Container>
      <Header>
    <Left>
        <Button transparent onPress={ () => this.props.navigation.goBack()}>
          <FontAwesomeIcon icon={faArrowLeft} style={styles.icon} size={20}/> 
        </Button>
    </Left>
    <Body>
          <Title>Progress Report</Title>
    </Body>
    <Right /> 
    
    </Header>
      <Content>
        <View>
        <View>
        <List>
              {
                this.props.student.map((stud,i)=>{
                  return(
                  <ListItem avatar key={i}>
                    <Left>
                      <Thumbnail source={(stud.image)}/>
                    </Left>
                    <Body >
                      <Text style={styles.titleText}>
                        {stud.firstName} {stud.lastName}
                      </Text>
                      <Text note>Class:{stud.class}</Text>
                    </Body>
                    <Right>
                    <Button  small success >
                        <Text  onPress={()=>this.props.navigation.navigate('Result')}   style={color='#000'}>View Result</Text>
                    </Button>
                  </Right>
                  </ListItem>
                ); 
                })
              }
                  
            </List>
          </View>
          
        
     </View>
    
      </Content>
    
</Container>
   
    )
  }
}
 
const styles = StyleSheet.create({
  container: { flex: 1, padding: 16, paddingTop: 30, backgroundColor: '#fff' },
  head: {  height: 40,  backgroundColor: '#f1f8ff'  },
  wrapper: { flexDirection: 'row' },
  title: { flex: 1, backgroundColor: '#f6f8fa' },
  row: {  height: 30 },
  text: { textAlign: 'center' },
  icon:{
    color:'#FFF',
    paddingBottom:90,
    paddingLeft:10,
  },
  titleText:{
    
    fontFamily: 'Cochin',
    fontWeight: 'bold'
}
});


const mapStateToProps=(state) =>{

  return {
      student:state.student.studentList
      
  }
}


const mapDispatchToProps=(dispatch) =>{
 
  return {
      
    }
}

export default connect(mapStateToProps, mapDispatchToProps) (ProgressReportPage)

