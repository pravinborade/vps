import React, { Component } from "react";
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faArrowLeft, faSchool } from '@fortawesome/free-solid-svg-icons';
import {StyleSheet} from 'react-native';

import { Container, Header, Content, Card, CardItem, Text, Body,Left,Right,Button,Title,Icon } from "native-base";
export default class NotificationPage extends Component {
  render() {
    return (
      <Container>
        
    <Header>
    <Left>
    <Button transparent onPress={ () => this.props.navigation.goBack()}>
        <FontAwesomeIcon icon={faArrowLeft} style={paddingLeft = 15} size={20} color="white" />
    </Button>
    </Left>
    <Body>
    <Title>Notification</Title>
    </Body>
    <Right/>
  </Header>
        
    <Content padder>
    <Card>
        <CardItem>
            <Body>
                <Text style={styles.titleText}>
                    News Update
                </Text>
            </Body>
            
        </CardItem>
    </Card>
    <Card>
        <CardItem>
          <Body>
            <Text style={styles.titleText}>
                Resource Update
            </Text>
          </Body>
        </CardItem>
            
    </Card>
    <Card>
        <CardItem>
            <FontAwesomeIcon icon={faSchool} style={paddingLeft = 15} size={25} color="#4682b4" />
              <Text style={styles. titleText}>Office Related</Text>
              <Right>
                <Icon name="arrow-forward" />
              </Right>
        </CardItem>
    </Card>
    <Card>
        <CardItem>
            <FontAwesomeIcon icon={faSchool} style={paddingLeft = 15} size={25} color="#4682b4" />
              <Text style={styles. titleText}>School Related</Text>
              <Right>
                <Icon name="arrow-forward" />
            </Right>
        </CardItem>
    </Card>
    <Card transparent>
        <CardItem>
        <Button rounded small success>
            <Text>About Us</Text>
        </Button>
        </CardItem>
    </Card>
    <Card transparent>
        <CardItem>
        <Button rounded small  success>
            <Text>Help</Text>
        </Button>
        </CardItem>
    </Card>
    
   
  </Content>
</Container>
    );
  }
}

const styles = StyleSheet.create({

    container: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center",
      backgroundColor: '#fff',
    },
    titleText:{
        textAlign:'center',
        paddingLeft:30,
        fontFamily: 'Cochin',
        fontWeight: 'bold'
    }

})