import React, { Component } from 'react';
import { Container, Header, Content, List, ListItem, Left, Body, Right, Thumbnail, Text, Button, Title } from 'native-base';
import { StyleSheet, View } from 'react-native';
import { toHtml } from '@fortawesome/fontawesome-svg-core';
import { connect } from 'react-redux';
import axios from 'axios';
import { baseURL, jsonHeader } from '../../Config';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faArrowLeft, faPlus } from '@fortawesome/free-solid-svg-icons';



class UserListPage extends Component {

  constructor(props) {
    super(props);
    this.state = {

    }

  }




  componentDidMount() {
    axios.post(`${baseURL}/api/user/create`, jsonHeader)
      .then(res => res.json())
      .then(
        (result) => {
          // this.props.addUser(result);
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      )
  }
  render() {

    return (
      <Container>
        <Header>
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()} >
              <FontAwesomeIcon icon={faArrowLeft} style={styles.icon} size={20} />
            </Button>
          </Left>
          <Body>
            <Title>User List</Title>
          </Body>
          <Right>
            <FontAwesomeIcon icon={faPlus} size={20} style={styles.IconColor} onPress={() => this.props.navigation.navigate('AddAminAccount')} />
          </Right>
        </Header>
        <Content>
          {/* <View>
            <List>
              {
                this.props.user.map((user1, i) => {
                  return (
                    <ListItem  key={i}>

                      <Left />
                      <Body >
                        <Text style={styles.titleText}>
                          {user1.firstName} {user1.lastName}
                        </Text>

                      </Body>
                      <Right />
                    </ListItem>
                  );
                })
              }

            </List>
          </View> */}
        </Content>
      </Container>
    );
  }
}



const styles = StyleSheet.create({

  icon: {
    color: '#FFF',
    paddingBottom: 0,
    paddingLeft: 10,


  },
  titleText: {

    fontFamily: 'Cochin',
    paddingBottom: 0,


  },
  IconColor: {
    color: '#FFF',
  },
})


const mapStateToProps = (state) => {

  return {

    // user:state.user.usersList

  }
}


const mapDispatchToProps = (dispatch) => {

  return {



  }

}


export default connect(mapStateToProps, mapDispatchToProps)(UserListPage)
