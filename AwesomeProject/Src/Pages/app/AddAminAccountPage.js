import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableHighlight,
  Image,
  Alert
} from 'react-native';
import {
    Container, Header,
    Title, Left, Icon, Right,
    Button, Body, Content,
    Footer,
    FooterTab 
  } from "native-base";

import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import  AddAdminForm  from '../../Components/app/forms/AddAdminForm';
import axios from 'axios';
import {baseURL,jsonHeader} from '../../Config';
import AppHeader from '../../Components/AppHeader';
import {saveUser} from   '../../Actions/authActions';


class AddAminAccountPage extends Component {
    constructor(props) {
      super(props);
      this.state = {
        phone_number  : '',
       
        formError:[],
      }
    }
     submit = values => {

      this.addNewUser(values);
    }

    addNewUser(values){
      debugger;
      
      formData={
        
        'phoneNumber':values.phone_number,
        'firstName':values.firstname,
        'lastName':values.lastname,

       
      }
      return axios.post(`${baseURL}/api/user/create`,formData,jsonHeader)
      .then((response) => {
           debugger;
           
           this.props.saveUser(response.data)
         
      })
      .catch((error) => {
            debugger;
        if(error.response.data.fieldErrors.length > 0){
          var err=[];
  
          for(let i=0;i<error.response.data.fieldErrors.length>0;i++){
            err.push(error.response.data.fieldErrors[i].message);
          }
          
          this.setState({
            ...this.state,
            formError:err
          })
          
        }
      })
      .finally(function () {

      });

    }

    saveUser = (data) => {
      this.props.saveUser(data)
          .then(() => {
            if(response.status>=200)
        this.props.navigation.navigate('UserList');
        
          })
          .catch((error) => {
              this.setState({ error })
          })
    };

  
      
    render() {
      
  
       return   (
         <Container>
              <AppHeader  title={"Add New Account"} navigation={this.props.navigation}/>
         
        <Content >
            <View>
             
              <AddAdminForm   onSubmit={this.submit} formErrors={this.state.formError} />
            </View>
        </Content> 
    </Container>
        );
    }
}
 

const styles= StyleSheet.create({

 
    
    title:{
      fontSize:20,
      color:"#000",
      paddingTop:0
    },
  form:{
      flex:1,
      flexDirection:'column',
      justifyContent:'space-around',
      justifyContent:'center',
      alignItems:'center'

  }
    
   
})

const mapStateToProps=(state) =>{
  return {  }
}


const mapDispatchToProps=(dispatch) =>{
  return {
  
   saveUser:(data) => dispatch(saveUser(data)),

   
   
  }
}
export default connect(mapStateToProps, mapDispatchToProps) (AddAminAccountPage)