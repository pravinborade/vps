import React, { Component } from 'react';
import {StyleSheet} from 'react-native';

import { Container, Header, 
  Title, Left, Icon, Right, 
  Button, Body, Content, 
  Footer,
  FooterTab,Text, View
} from "native-base";
import { connect } from 'react-redux';


import ContactForm from '../../../Src/Components/app/forms/ContactForm';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import {faArrowLeft} from '@fortawesome/free-solid-svg-icons';
import {updateContact} from '../../../Src/Actions/contactAction';

class ContactPage extends Component{


  constructor(props){
    debugger;
        super(props);
        
        this.state={
            clicked:0,
            formEdit:false,
        
        }
        
    }


    editForm = () => {
              this.setState({
                ...state,
                
                formEdit: ! this.state.formEdit
              })
      }
    
    submit = values => {
      debugger;
                console.log(values);
                this.props.updateContact(values);       
                this.editForm();
                 
    }

    render() {
        return (
        <Container>
            <Header>
                <Left>
                    <Button transparent onPress={ () => this.props.navigation.goBack()}>
                        <FontAwesomeIcon icon={faArrowLeft} style={styles.icon}/> 
                    </Button>
                </Left>
                <Body>
                    <Title>Emegency Contact</Title>
                </Body>
                <Right>
                  {
                    this.state.formEdit
                      ?
                    <Button transparent onPress={ () => this.editForm()}>
                      <Text>
                      CANCEL
                      </Text> 
                    </Button>
                  
                      :  
                    <Button transparent onPress={ () => this.editForm()}>
                      <Text>
                      EDIT
                      </Text> 
                    </Button>
                  
                  }
                  
                </Right>
          </Header>
            <Content>
           
                {
                  this.state.formEdit 
                    ?
                    <ContactForm onSubmit={this.submit} initialValues={this.props.contact}></ContactForm>  
                    :
                  <View style={{padding:30}}>

                    <Text  style={styles.userInfo}>{this.props.contact.mobileNumber}</Text>
                    <Text  style={styles.userInfo}>{this.props.contact.secondaryContact}</Text>
                 
                  
                  
                  </View>
                }
              
            </Content>
            
        </Container>

        );
    }
}


styles = StyleSheet.create({

    container:{
        flex:1, 
        justifyContent:"center",
        alignItems:"center",
        
    },
    userInfo:{
        flex:1,
        justifyContent:'center',
        textAlign:'center',
        width:150,
        height:40,
        color:'green',
        borderBottomColor:'red',
        alignSelf:'center'
        

      },
      icon:{
          color:'white',
      }


})


const mapStateToProps=(state) =>{
 
    return {
        contact:state.contact.contact
    }
  }
  
  const mapDispatchToProps=(dispatch) =>{
    return {
            
            updateContact:(contact)=>{dispatch(updateContact(contact))}
          
    }
  }

  export default connect(mapStateToProps, mapDispatchToProps) (ContactPage)
