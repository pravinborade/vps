import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import {
  Container, Header,
  Title, Left, Icon, Right,
  Button, Body, Content,
  Footer,
  FooterTab, Text, View
} from "native-base";
import { connect } from 'react-redux';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faArrowLeft,faCamera } from '@fortawesome/free-solid-svg-icons';
import StudentsForm from '../../Components/app/Student/PersonalDetailForm'
import { updateStudent } from './../../Actions/studentActions';
import {addStudent} from './../../Actions/studentActions';

class StudentPersonalInfoPage extends Component {

  constructor(props) {
    

    super(props);
    this.state = {
      clicked: 0,
      formEdit: false,

      avatarSource: null,
    }
  }


  onDateChange =  (date) => {
    
    this.props.input.onDateChange(moment(date));

  }



  editForm = () => {
    
    this.setState({
      ...state,
      formEdit: !this.state.formEdit
    })
  }


  changeAction(buttonIndex) {
    this.setState({ clicked: BUTTONS[buttonIndex] });
  }

  submit = values => {
    
        console.log(values);
    this.props.updateStudent(values);
    this.editForm();
  }

  render() {
    return (
      <Container>
        <Header>
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <FontAwesomeIcon icon={faArrowLeft} style={styles.icon} size={20}/>
            </Button>
          </Left>
          <Body>
            <Title> Student Personal Information</Title>
          </Body>
          <Right>
            {
              this.state.formEdit
                ?
                <Button transparent onPress={() => this.editForm()}>
                  <Text>
                    CANCEL
                   </Text>
                </Button>
                :
                <Button transparent onPress={() => this.editForm()}>
                  <Text>
                    EDIT
                  </Text>
                </Button>

            }

          </Right>
        </Header>
        <Content>

          {
            this.state.formEdit
              ?
              <StudentsForm onSubmit={this.submit} initialValues={this.props.student} onDateChange={this.onDateChange}  />
              :
              <View style={{ padding: 30 }}>
                <Text style={styles.userInfo} >{this.props.student.firstName} </Text>
                <Text style={styles.userInfo} >{this.props.student.middleName} </Text>
                <Text style={styles.userInfo} >{this.props.student.lastName} </Text>
                <Text style={styles.userInfo}>{this.props.student.gender}</Text>
                <Text style={styles.userInfo}>{this.props.student.relision}</Text>
                <Text style={styles.userInfo}>{this.props.student.cast}</Text>
                <Text style={styles.userInfo}>{this.props.student.subCast }</Text>
                <Text style={styles.userInfo}>{this.props.student.nationality }</Text>
               
              </View>
          }

        </Content>

      </Container>

    );
  }
}

const styles = StyleSheet.create({

  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",

  },
  userInfo: {
    flex: 1,
    justifyContent: 'center',
    textAlign: 'center',
    width: 150,
    height: 40,
    fontWeight:'Cochin',
    fontWeight:'bold',
    borderBottomColor: 'red',
    fontSize:20,


  },
  icon: {
    color: 'white',
  }

})



const mapStateToProps = (state) => {
   
  return {
    student: state.student.student

  }
}


const mapDispatchToProps = (dispatch) => {
 
  return {

    
    updateStudent: (student) => { dispatch(updateStudent(student)) }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(StudentPersonalInfoPage)

