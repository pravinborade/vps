import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Button,
  TouchableHighlight,
  Image,
  Alert
} from 'react-native';
import  AuthForm  from '../../Components/auth/AuthForm';

class ForgetPasswordPage extends Component {

    constructor(props) {
      super(props);
      this.state = {
        phone_number  : '',
        password: '',
      }
    }
  
    submit = values => {
       
        if(values. phone_number  == '8888141341'){
            this.props.navigation.navigate('Password')
          }
    }
    render() {
        return (
          <View style={styles.container}>
               <View >
                <Image style={styles.logoImg}
                  style={{width: 150, height: 150}}
                  source={require('../../Assets/vps-new-logo.png')}
                 />
                </View> 
                <View>
                   <AuthForm formType={"Reset"} onSubmit={this.submit}/>
                </View>

                   <TouchableHighlight  onPress={() => this.props.navigation.navigate('Password')}>
                          { <Text 
                        
                             
                          > </Text> }
                   </TouchableHighlight>

            </View>
        );
    }
}


const styles= StyleSheet.create({

    container:{
        flex:1,
        justifyContent:"center",
        alignItems:"center",
        
    },
    logoImg:{
        marginTop:30,
        justifyContent:'center'
    }
})

const mapStateToProps=(state) =>{
  return {  }
}


const mapDispatchToProps=(dispatch) =>{
  return {
    addUser:(user) => {dispatch(addUser(user))},
  }
}
  export default ForgetPasswordPage