import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Button,
  TouchableHighlight,
  Image,
  Alert
} from 'react-native';
import { connect } from 'react-redux';
import axios from 'axios';
import {baseURL,jsonHeader} from '../../Config';
import {KeyboardAvoidingView} from 'react-native';
import  PasswordForm from '../../Components/auth/PasswordForm';





class PasswordPage extends Component {

    constructor(props) {
    
      super(props);
      this.state = {
        conform_password  : '',
        password: '',
        error:false,
       
      }
    }
        
    submit = values => {
      
     
      if(values.password  == values.conform_password ){
        this.props.navigation.navigate('Home')
      }else {
          
            this.setState({
                ...this.state,
                error:true
            })
      } 
       
    }
     
    //   formData={
    //     'phone_number':values.phone_number,
    //     'password':values.password
         



    //   }
    //   return axios.post(`${baseURL}/api/login/`,formData,jsonHeader)
    //   .then((response) => {
    //       this._signInAsync(response.data.data.token)
    //   })
    //   .catch((error) => {
      
    //   })
    //   .finally(function () {
          
         
    //   });


           
    // }

    // _signInAsync = (token) => {
    //   this.props.saveUserToken(token)
    //       .then(() => {
    //     this.props.navigation.navigate('Home');
    //     this.props.navigation.navigate('');
    //       })
    //       .catch((error) => {
    //           this.setState({ error })
    //       })
    // };

    // getToken = async() => {
    //   await AsyncStorage.getItem("userToken",function(){
        
    //     this.props.navigation.navigate('Home');
  
    //   })
    //  }
    render() {
        return (
           <View style={styles.container }>
                <View >
                <Image
                  style={{width: 100, height: 100}}
                  source={require('../../Assets/vps-new-logo.png')}
                />
                </View> 
              <View >
                {<Text style ={styles.btnText}    
                 
                 >RESET PASSWORD</Text>
                }
              </View>

                <View style={{flex:1}}>
                   <PasswordForm  onSubmit={this.submit} formError={this.state.error}/>
                   
                </View>
               
             
            
            </View>
           
        );
    }
}
 

const styles= StyleSheet.create({

    container:{
        flex:1,
        justifyContent:"center",
        alignItems:"center",
        
    },
    
    btnText:{
      fontSize:20,
      color:"#000",
      paddingTop:30
    },
    logoName:{
     height:100,
     width:100,
     marginTop:10
       
    },
    btnForget:{
      color:'blue',
      justifyContent:'center',
      alignItems:'center',
      fontSize:20,
      fontFamily: 'AvenirNext-Bold',
      marginBottom:170
    }
})



const mapStateToProps=(state) =>{
  return {  }
}


const mapDispatchToProps=(dispatch) =>{
  return {
    
  }
}



export default connect(mapStateToProps, mapDispatchToProps) (PasswordPage)