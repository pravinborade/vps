import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Button,
  TouchableHighlight,
  Image,
  
  Alert
} from 'react-native';


import {connect} from 'react-redux';
import {baseURL,jsonHeader} from '../../Config';
import axios from 'axios';


import  SingUpForm  from '../../Components/auth/SignUpForm';
import LogInPage from './LogInPage';


class SignUpPage extends Component {

    constructor(props) {
   
      super(props);

      this.state = {
        phoneNumber: '',
        password: '',
        conformPassword:'',
        formError:[],
      }

    }

    submit = values => {
     
      this.signup(values);     

    
      
  }
 

  
  signup(values){
   
    formData={
      'phoneNumber':values.phone_number,
      'password':values.password,
      'confirmPassword':values.conformPassword,
      
    }
    return axios.post(`${baseURL}/user/add`,formData,jsonHeader)
    .then((response) => {
        this._signInAsync(response.data.data.token)
    })
    .catch((error) => {
      if(error.response.data.fieldErrors.length > 0){
        var err=[];

        for(let i =0;i<error.response.data.fieldErrors.length>0;i++){
          err.push(error.response.data.fieldErrors[i].message);
        }
        
        this.setState({
          ...this.state,
          formError:err
        })
        
      }
      
    })
    .finally(function () {
        
       
    });
  }


    render() {
    
      return (
           <View style={styles.container}>
                <View  >
                <Image style={styles.logoImg}
                  source={require('../../Assets/vps-new-logo.png')}
                 />
                </View> 
                <View >
                {<Text style ={styles.title}    
                 
                 >SIGNUP</Text>
                }
              </View>
               <SingUpForm  onSubmit={this.submit} formErrors={this.state.formError}/>
           </View>
        );
    }
}


    const styles= StyleSheet.create({

      container:{

          flex:1,
          justifyContent:"center",
          alignItems:"center",
         
           
      },

     title: {
        fontSize:20,
        color:"#000",
        paddingTop:0
      },
      logoImg:{
        marginTop:10,
        height:100,
        width:100,
        marginTop:10

      }

    })


    const mapStateToProps=(state) =>{
      return {  }
    }
    
    
    const mapDispatchToProps=(dispatch) =>{
      return {

      }
    }

  export default connect(mapStateToProps,mapDispatchToProps)(SignUpPage);