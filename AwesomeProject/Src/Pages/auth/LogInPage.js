import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Button,
  TouchableHighlight,
  Image,
  Alert
} from 'react-native';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import  AuthForm  from '../../Components/auth/AuthForm';
import axios from 'axios';
import {baseURL,jsonHeader} from '../../Config';
import {KeyboardAvoidingView} from 'react-native';
import {saveUserToken} from '../../Actions/authActions';



class LogInPage extends Component {
    constructor(props) {
      super(props);
      this.state = {
        phone_number  : '',
        password: '',
        formError:[],
      }
    }
    submit = values => {

      // if(values. phone_number  == '8888141341' && values. password =='123456'){
      //   this.props.navigation.navigate('Home')
      // }
      this.login(values);
    }

    login(values){
      
      formData={
        'phoneNumber':values.phone_number,
        'password':values.password
      }
      return axios.post(`${baseURL}/api/login`,formData,jsonHeader)
      .then((response) => {
          this._signInAsync(response.data.authenticationToken)
         
      })
      .catch((error) => {

        if(error.response.data.fieldErrors.length > 0){
          var err=[];
  
          for(let i =0;i<error.response.data.fieldErrors.length>0;i++){
            err.push(error.response.data.fieldErrors[i].message);
          }
          
          this.setState({
            ...this.state,
            formError:err
          })
          
        }
      })
      .finally(function () {

      });

    }

           
    // }

    _signInAsync = (token) => {
      this.props.saveUserToken(token)
          .then(() => {
        this.props.navigation.navigate('AuthLoading');
        this.props.navigation.navigate('');
          })
          .catch((error) => {
              this.setState({ error })
          })
    };

    getToken = async() => {
      await AsyncStorage.getItem("userToken",function(){
        this.props.navigation.navigate('Home');
      })
      }

    render() {
        return (
      <View style={styles.container}>
      <View >
          <Image
            style={{width: 100, height: 100}}
            source={require('../../Assets/vps-new-logo.png')}
          />
      </View> 
      <View >
        {<Text style ={styles.title}>LOGIN</Text>
        }
      </View>
      <View>
          <AuthForm  formType={"LOGIN"}  onSubmit={this.submit} formErrors={this.state.formError}/>
      </View>                 
  </View>
        );
    }
}
 

const styles= StyleSheet.create({

    container:{
        flex:1,
        justifyContent:"center",
        alignItems:"center",
        
    },
    
    title:{
      fontSize:20,
      color:"#000",
      paddingTop:0
    },
    logoName:{
     height:100,
     width:100,
     marginTop:10
       
    },
    // btnForget:{
    //   color:'blue',
    //   alignItems:'center',
    //   justifyContent:'center',
    //   fontSize:20,
    //   marginTop:10,
    // }
})



const mapStateToProps=(state) =>{
  return {  }
}


const mapDispatchToProps=(dispatch) =>{
  return {

    saveUserToken:(token) => dispatch(saveUserToken(token)),
  }
}



export default connect(mapStateToProps, mapDispatchToProps) (LogInPage)