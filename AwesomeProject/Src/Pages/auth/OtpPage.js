import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Button,
  TouchableHighlight,
  Image,
  Alert
} from 'react-native';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import OtpInputs from 'react-native-otp-inputs'


import AwesomeButton from "react-native-really-awesome-button";

import axios from 'axios';
import {baseURL,jsonHeader} from '../../Config';


import {saveUserToken} from '../../Actions/authActions';
import OtpForm from '../../Components/auth/OtpForm';



class OtpPage extends Component {
    
    constructor(props) {
      
      super(props);
      this.state = {
        otp : '',
        phone_number:'8888141341',
       
      }
     
    }

    handleChange=(otp)=>{
   
        this.setState({otp :otp})
    }
  

   

   
    submit = values => {
     
      console.log(values);
      
      
      
      if(this.state.otp=='123456')
      {
        this.props.navigation.navigate('StudentPersonalInfo');
      }
     




      // formData={
      //   'otp':values.otp,
      // }
      // return axios.post(`${baseURL}/api/login/`,formData,jsonHeader)
      // .then((response) => {
      //     this._signInAsync(response.data.data.token)
      // })
      // .catch((error) => {
      
      // })
      // .finally(function () {
          
         
      // });


           
    }

    _signInAsync = (token) => {
      this.props.saveUserToken(token)
          .then(() => {
              this.props.navigation.navigate('App');
          })
          .catch((error) => {
              this.setState({ error })
          })
    };

    getToken = async() => {
      await AsyncStorage.getItem("userToken",function(){
        
        this.props.navigation.navigate('AuthLoading');
  
      })
    }
    render() {

        return (
           <View style={styles.container}>
              <Text style={styles.textAlign}>VERIFY DETAILS</Text>
                <View style={{flex:1}}>
                  <OtpForm handleChange={this.handleChange} onSubmit={this.submit} otp={this.state.otp}/>
                </View>   
            </View>
            
        );
    }
}


const styles= StyleSheet.create({

    container:{
        flex:1,
        justifyContent:"center",
        alignItems:"center",  
    },
    btnText:{
      fontSize:20,
      color:"#000",
      flexDirection:"row",
    },
    btnAlign:{
      justifyContent:'center',
    },
    textAlign:{
      fontFamily:'bold',
        fontSize:30,
        textAlign:'center',
        justifyContent:'center',
        marginTop:50
    },
    input:{
      alignItems:'center',
      justifyContent:'center'
    },
})



const mapStateToProps=(state) =>{
  return {  }
}


const mapDispatchToProps=(dispatch) =>{
  return {
   
  }
}



export default connect(mapStateToProps, mapDispatchToProps) (OtpPage)