import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Button,
  TouchableHighlight,
  Image,
  Alert
} from 'react-native';
import  {PasswordForm} from '../../Components/auth/PasswordForm';

class ForgetPasswordPage extends Component {

    constructor(props) {
      super(props);
      this.state = {
       phone_number : '',
        password: '',
        conformPassword:'',
      }
    }
  
    submit = values => {
    
      this.forgetPassword(values);
    }
     
    forgetPassword(values){


      formData={
        'phoneNumber':values.phone_number,
        'password':values.password,
        'conformPassword':values.conformPassword
      }
      return axios.post(`${baseURL}/api/login`,formData,jsonHeader)
      .then((response) => {
          this._signInAsync(response.data.data.token)
      })
      .catch((error) => {

        if(error.response.data.fieldErrors.length > 0){
          var err=[];
  
          for(let i =0;i<error.response.data.fieldErrors.length>0;i++){
            err.push(error.response.data.fieldErrors[i].message);
          }
          
          this.setState({
            ...this.state,
            formError:err
          })
          
        }
      })
      .finally(function () {
          
         
      });



     }
     

    render() {
        return (
          <View style={styles.container}>
                <View>
                   <PasswordForm  onSubmit={this.submit} formType={Reset} formErrors/>
                </View>

                   <TouchableHighlight>
                          { <Text 
                        
                              onPress={() => this.props.navigation.navigate('Otp')}
                          > </Text> }
                   </TouchableHighlight>

            </View>
        );
    }
}


const styles= StyleSheet.create({

    container:{
        flex:1,
        justifyContent:"center",
        alignItems:"center",
        
    },
})

const mapStateToProps=(state) =>{
  return {  }
}


const mapDispatchToProps=(dispatch) =>{
  return {
    addUser:(user) => {dispatch(addUser(user))},
  }
}
  export default ForgetPasswordPage