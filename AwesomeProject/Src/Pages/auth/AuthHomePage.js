import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Button,
  TouchableHighlight,
  Alert,
  Image
} from 'react-native';
import AwesomeButton from "react-native-really-awesome-button";




class AuthHomePage extends Component {

    constructor(props) {
      super(props);
      state = {
        
      }
    }

    render() {
        return (

            <View style={styles.container }>
                
                
                <Image
                  style={styles.logoName}
                  source={require('../../Assets/vps-new-logo.png')}
                />
                
                <View  style={[styles.btnAlign,{flex:1}]}>
                     
                          <AwesomeButton
                            progress
                            backgroundProgress='green'
                            width={350}
                            height={50}
                            progressLoadingTime={10}
                            onPress={() => this.props.navigation.navigate('LogIn')}
                          >
                        <Text style={styles.btnText}>LOGIN</Text>
                        </AwesomeButton>
                        
                 </View>

                 {/* <View style={styles.hairline} />
                  <Text style={styles.loginButtonBelowText1}>OR</Text>
                  <View style={styles.hairline} />
                <View style={styles.signUpBtn}> 

                 <AwesomeButton
                      progress
                      backgroundProgress='green'
                      width={350}
                      height={50}
                      progressLoadingTime={10}
                      onPress={() => this.props.navigation.navigate('SignUp')}
                    >
                  <Text style={styles.btnText}>Create New Account</Text>
                  </AwesomeButton>
                   
                </View>       */}
            </View>
        );
    }
}


const styles = StyleSheet.create({

    container:{

        flex:1,
        justifyContent:"center",
        alignItems:"center",
        flexDirection:"column"
    },
      btnText: {
        color: 'white',
        fontSize:20,
        textAlign:'center',
      },
      logoName:{
       height:100,
       width:100,
       marginTop:20
       
      },
      btnAlign:{
        justifyContent:'center',
        alignItems:'center'
      },
      signUpBtn:{
        justifyContent:'center',
        marginBottom:100,
        flex:1
      },
      hairline:{
       
        backgroundColor: '#A2A2A2',
        height: 2,
        width: 165
      },
      loginButtonBelowText1: {
        fontFamily: 'AvenirNext-Bold',
        fontSize: 14,
        paddingHorizontal: 5,
        alignSelf: 'center',
        color: '#A2A2A2'
      },

    });    

 export default AuthHomePage 