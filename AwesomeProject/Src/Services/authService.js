import AsyncStorage from "@react-native-community/async-storage";



export async function saveToken(token) {
    await AsyncStorage.setItem("userToken",token)
}


export async function getToken(token) {
    await AsyncStorage.getItem("userToken",token)
}

export async function deleteToken(token) {
    await AsyncStorage.deleteItem("userToken",token)
}

