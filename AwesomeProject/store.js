import { createStore, applyMiddleware, compose } from 'redux';
import ReduxThunk from 'redux-thunk';
import rootReducer from './Src/Reducers/rootReducer'

const middlewares = [ReduxThunk];

import {createLogger} from 'redux-logger';

const loggerMiddleware = createLogger()


const store = createStore(
   rootReducer,
   {},// default state of the application
   compose(applyMiddleware(...middlewares,loggerMiddleware)),
);


export default store;
